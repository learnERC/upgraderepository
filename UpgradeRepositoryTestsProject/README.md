# Set di benchmark per problematiche di tipo "Data Loss", "Upgrade del framework", "Resource leak", "Energy bug" e "Performance bug".
Questo repository contiene i test, creati con Appium, che riproducono i bug "DATA LOSS", "UPGRADE", "RESOURCE LEAK", "ENERGY BUG" e "PERFORMANCE BUG" di diverse applicazioni Android cercate su github.
	
<br><br>
INSTRUZIONI:<br>
<ol>
    <li> Clonare il repository sul pc.</li>
	<li> Creare o importare la macchina virtuale. Ho utilizzato genymotion come SW per virtualizzare Android sul pc.</li>
	<li> Scaricare e installare "Appium".</li>
	<li> Configurare "Appium" con indirizzo "127.0.0.1" e porta "4444". Selezionare la versione di Android che si vuole utilizzare e alla voce "DeviceName" inserire "AndroidTestDevice".</li>
	<li> Scaricare e installare il SW "IntelliJ IDEA".</li>
	<li> Aprire il progetto.</li>
	<li> Avviare l'emulatore desiderato e "Appium" (attenzione quale versione di Android è settata nelle impostazioni).</li>
	<li> Installare l'apk del programma da testare sulla macchina virtuale (tutti gli apk sono presenti nella cartella "/source_projects").</li>
	<li> Se avete importato la macchina virtuale non occorre installare nulla.</li>
	<li> Compilare i test (la cartella out non viene sincronizzata, occorre quindi compilarli).</li>
	<li> Avviare il test.</li>
</ol>
<br>
All'interno del repository è presente un file excel che raccoglie tutti i bug trattati. Questi è stata tenuta traccia delle ricerche, link ai test, link alle issue, e altro.<br>
Le macchine virtuali verranno caricate in un secondo momento.


<br><br>
INSTRUZIONI Resource Leak, Energy Bug, Performance Bug:<br>
<ol>
    <li> Clonare il repository sul pc.</li>
	<li> Creare la macchina virtuale tramite Genymotion. Il dispositivo Emulato è un Google Nexus 5 con versione 6.0 di Android.</li>
	<li> Installare i Google service sull’emulatore tramite l’opzione di Genymotion. (Alcune applicazioni richiedono questi servizi.</li>
	<li> Scaricare e installare "Appium".</li>
	<li> Configurare "Appium" con indirizzo "127.0.0.1" e porta "4444". Impostare la versione di Android su 6.0 Marshmallow (API 23) e alla voce "DeviceName" inserire "AndroidTestDevice".</li>
	<li> Scaricare e installare il SW "IntelliJ IDEA".</li>
	<li> Aprire il progetto.</li>
	<li> Avviare l'emulatore desiderato e "Appium" (attenzione quale versione di Android è settata nelle impostazioni).</li>
	<li> Installare l'apk del programma da testare sulla macchina virtuale (tutti gli apk sono presenti nella cartella "/source_projects").</li>
	<li> Compilare i test (la cartella out non viene sincronizzata, occorre quindi compilarli).</li>
	<li> Avviare il test.</li>
</ol>
<br>
