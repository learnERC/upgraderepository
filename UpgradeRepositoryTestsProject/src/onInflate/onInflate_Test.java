package onInflate;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import libs.AndroidHelpFunction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import static libs.AndroidHelpFunction.runtimeExec;


public class onInflate_Test {
    AppiumDriver driver;
    String AppName = "Example_fragment_oninflate";
    String APP_PACKAGE = "dk.example.example_fragment_oninflate";
    String APP_ACTIVITY = "dk.example.example_fragment_oninflate.MainActivity";
    //String AccountGoogle = "christianrussi@gmail.com";
    Integer Sleep = 1500; //I have to set a sleep because the test is too fast to open the SMS app
    AndroidHelpFunction androidHelpFunction;
    String tracePath = "/data/data/" + APP_PACKAGE + "/files/";
    String adbPath = "/home/christian/Android/Sdk/platform-tools/adb";

    @Before
    public void setUp() throws Exception{
        //service.start();
        //reader.readFile();
        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        //File apk = Paths.get(".", "source_projects/UPGRADE/sms-backup-plus/bug", "sms-backup-plus-1_5_5-bug.apk").normalize().toFile();
        //cap.setCapability(MobileCapabilityType.APP, apk);
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);

        cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);

        //clear recent apps
        androidHelpFunction.clearRecentApps();
        //reset app
        androidHelpFunction.clearDataApp(AppName, APP_PACKAGE);
        //make SMS Messaging default app for SMS


        driver.closeApp();




    }


    @After
    public void tearDown() throws Exception{

        driver.quit();
    }

    @Test
    public void run() throws InterruptedException, IOException {
        long startTime = Instant.now().getEpochSecond();
        driver.launchApp();

        driver.rotate(org.openqa.selenium.ScreenOrientation.LANDSCAPE);
        Thread.sleep(5000);

        boolean present = androidHelpFunction.textIsPresent("Hello World");

        androidHelpFunction.clickOnHOME();
        System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
        TimeUnit.SECONDS.sleep(10);

        // restore rotation
        Runtime.getRuntime().exec(adbPath + " shell am start -a android.settings.APPLICATION_DETAILS_SETTINGS package:" + APP_PACKAGE);
        androidHelpFunction.waitForId("com.android.settings:id/right_button");
        driver.rotate(org.openqa.selenium.ScreenOrientation.PORTRAIT);

        if(present) {
            assert true;
        } else {
            assert false;
        }


    }
}