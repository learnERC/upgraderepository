package poGoIV;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import libs.AndroidHelpFunction;
import libs.AndroidHelpFunctionALT;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.URL;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import static libs.AndroidHelpFunction.runtimeExec;

public class GOIV_Test {
    AndroidDriver driver;

    String AppName = "GoIV";
    String APP_PACKAGE = "com.swanberg.pogoiv";
    String APP_ACTIVITY = "com.kamron.pogoiv.MainActivity";

    String tracePath = "/data/data/" + APP_PACKAGE + "/files/";
    String adbPath = "/home/christian/Android/Sdk/platform-tools/adb";

    AndroidHelpFunctionALT androidHelpFunction;
    //AndroidDriver androidDriver;

    @Before
    public void setUp() throws Exception{
        runtimeExec(adbPath + " shell rm -r " + tracePath);
        System.out.println("Old traces removed.");
        //service.start();
        //reader.readFile();
        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunctionALT(driver);
        //androidDriver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        //wait the activity
        cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);
        androidHelpFunction.clearRecentApps();
        driver.closeApp(); //driver doesn't know that the app was closed
        runtimeExec(adbPath + " shell rm -r " + tracePath);
        System.out.println("Old traces removed.");
    }


    @After
    public void tearDown() throws Exception{

        driver.quit();
    }

    @Test
    public void run() throws InterruptedException {
        long startTime = Instant.now().getEpochSecond();
        //return on app
        driver.launchApp();
        System.out.println("App Launched");

        Thread.sleep(5000);
        //androidHelpFunction.waitForIdAndClick("android:id/button2"); // Android Tracer Enabler
        androidHelpFunction.waitForIdAndClick(APP_PACKAGE + ":id/settings");

        androidHelpFunction.waitForTextAndClick("Check for update now");
        androidHelpFunction.waitForIdAndClick("android:id/button1");

        Thread.sleep(10000);
        //androidHelpFunction.waitForId("com.android.packageinstaller:id/ok_button"); // remove comment when testing on api 23
        if (androidHelpFunction.idIsPresent("android:id/aerr_restart")){
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
            assert false;
        } else if (androidHelpFunction.idIsPresent("com.android.packageinstaller:id/ok_button")){
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
            assert true;
        } else {
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
            assert false;
        }
    }
}

