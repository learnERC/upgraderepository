package inventoryAgent;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class InventoryAgent_5c49a23_Fix {

    AppiumDriver driver;

    final String APP_PACKAGE = "org.flyve.inventory.agent";

    @Before
    public void setUp() throws MalformedURLException {

        /* --- DEVICES USED ---
         * Device: Custom Phone
         * Android API: 6.0 - API 23
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: true
         * */

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE,  APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_PACKAGE + ".ui.ActivityMain");

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);

    }

    @Test
    public void testPresenceOfElements() throws InterruptedException {

        final String XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/";
        final String XPATH_INVENTORY = XPATH + "android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/" +
                "android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/" +
                "android.widget.LinearLayout[2]";
        final String XPATH_ELEMENTS = XPATH + "android.widget.RelativeLayout/android.support.v4.view.ViewPager/" +
                "android.widget.RelativeLayout/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout/" +
                "android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[1]";

        WebDriverWait wait = new WebDriverWait(driver, 120);

        // Wait for the permissions and allow them
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.koushikdutta.superuser:id/allow")));
        Thread.sleep(3000);

        driver.findElementById("com.koushikdutta.superuser:id/allow").click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(
                "com.android.packageinstaller:id/permission_allow_button")));

        driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(
                "com.android.packageinstaller:id/permission_allow_button")));

        driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(
                "com.android.packageinstaller:id/permission_allow_button")));

        driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_INVENTORY)));

        // Click the "Show my inventory" option and allow superuser permissions again
        driver.findElementByXPath(XPATH_INVENTORY).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.koushikdutta.superuser:id/allow")));
        Thread.sleep(3000);

        driver.findElementById("com.koushikdutta.superuser:id/allow").click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.koushikdutta.superuser:id/allow")));
        Thread.sleep(3000);

        driver.findElementById("com.koushikdutta.superuser:id/allow").click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(
                "com.android.packageinstaller:id/permission_allow_button")));

        driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(
                "com.android.packageinstaller:id/permission_allow_button")));

        driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(
                "com.android.packageinstaller:id/permission_allow_button")));

        driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();
        Thread.sleep(10000);

        // Check if there are the elements in the activity
        try {
            driver.findElementByXPath(XPATH_ELEMENTS);
            assert true;
        } catch (Exception e) {
            assert false;
        }


    }

    @After
    public void tearDown() {
        driver.quit();
    }

}