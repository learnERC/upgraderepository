package inventoryAgent;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import libs.AndroidHelpFunction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import static libs.AndroidHelpFunction.runtimeExec;

public class InventoryAgent_2aa01c9_Bug {

    AppiumDriver driver;

    final String APP_PACKAGE = "org.flyve.inventory.agent";
    AndroidHelpFunction androidHelpFunction;

    /*
     *  IMPORTANT:
     *  - BEFORE STARTING THE TEST, OPEN THE APP ONCE TO GIVE SUPERUSER PERMISSION FOREVER
     */

    @Before
    public void setUp() throws Exception {

        /* --- DEVICES USED ---
         * Device: Custom Phone
         * Android API: 5.1 - API 22
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: true
         * --------------------
         * Device: Custom Phone
         * Android API: 6.0 - API 23
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: false
         * */

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE,  APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_PACKAGE + ".ui.ActivityMain");

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);

    }

    @Test
    public void testPresenceOfElements() throws InterruptedException {
        long startTime = Instant.now().getEpochSecond();
        final String XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/";
        final String XPATH_INVENTORY = XPATH + "android.support.v4.widget.DrawerLayout/" +
                "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/" +
                "android.widget.ListView/android.widget.LinearLayout[2]";
        final String allowButton = "com.koushikdutta.superuser:id/allow";
        final String XPATH_SUPERUSER;

        boolean osVersionIs5_1 = driver.getCapabilities().getCapability("platformVersion").equals("5.1");
        if (osVersionIs5_1)
            XPATH_SUPERUSER = "/hierarchy/android.widget.FrameLayout/android.view.View/" +
                    "android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[1]";
        else
            XPATH_SUPERUSER = "/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/" +
                    "android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[1]";

        WebDriverWait wait = new WebDriverWait(driver, 360);
        //Thread.sleep(180000); // remove comment from this line when using UnimibLogger a2f2

        // Wait for the superuser permissions and allow them
        /*wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_SUPERUSER)));
        Thread.sleep(3000);
        driver.findElementById(allowButton).click();*/

        // Click the "Show my inventory" option
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_INVENTORY)));
        driver.findElementByXPath(XPATH_INVENTORY).click();

        // allow superuser permissions again
        /*wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_SUPERUSER)));
        Thread.sleep(3000);
        if (osVersionIs5_1) {
            driver.findElementById(allowButton).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_SUPERUSER)));
            Thread.sleep(3000);
        }
        driver.findElementById(allowButton).click();*/
        Thread.sleep(60000); // set 180 seconds when testing UnimibLogger a2f2

        // Check if there are the elements in the activity
        try {
            driver.findElementByXPath(XPATH + "android.widget.RelativeLayout/" +
                    "android.support.v4.view.ViewPager/android.widget.RelativeLayout/" +
                    "android.support.v7.widget.RecyclerView/android.widget.RelativeLayout/" +
                    "android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[1]");
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
            assert true;
        } catch (Exception e) {
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
            assert false;
        }

    }

    @After
    public void tearDown() {
        driver.quit();
    }

}