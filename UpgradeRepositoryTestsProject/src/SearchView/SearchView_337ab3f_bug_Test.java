package searchView;

import libs.AndroidHelpFunction;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.net.URL;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static libs.AndroidHelpFunction.runtimeExec;


public class SearchView_337ab3f_bug_Test {
    AppiumDriver driver;
    String AppName = "Bug-SearchView Sample";
    String APP_PACKAGE = "com.lapism.searchview.samplebug";
    String APP_ACTIVITY = "com.lapism.searchview.sample.activity.menu.ToolbarActivity";
    String tracePath = "/data/data/" + APP_PACKAGE + "/files/";

    AndroidHelpFunction androidHelpFunction;

    @Before
    public void setUp() throws Exception{
        //service.start();
        //reader.readFile();
        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);
        cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);

        //androidHelpFunction.clearRecentApps();

    }


    @After
    public void tearDown() throws Exception{

        driver.quit();
    }

    @Test
    public void run() throws InterruptedException {
        long startTime = Instant.now().getEpochSecond();
        try{
            androidHelpFunction.setTimeOut(120);
            androidHelpFunction.waitForId(APP_PACKAGE + ":id/searchEditText_input");
            androidHelpFunction.resetTimeOut();
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
            assert true;
        }catch (Exception e){
            androidHelpFunction.resetTimeOut();
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
            assert false;
        }
    }
}
