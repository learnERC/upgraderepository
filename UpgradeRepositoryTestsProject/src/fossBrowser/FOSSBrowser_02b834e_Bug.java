package fossBrowser;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidKeyCode;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FOSSBrowser_02b834e_Bug {

    AppiumDriver driver;

    final String APP_PACKAGE = "de.baumann.browser";

    /*
     *  IMPORTANT:
     *  - DISABLE PLAY PROTECT FROM GOOGLE PLAY STORE IN ANDROID 8.0 API 26 BEFORE INSTALLING TRACER ENABLER AND UNIMIB LOGGER
     *  - TURN OFF INTERNET, BOTH WIFI AND MOBILE DATA
     */

    @Before
    public void setUp() throws MalformedURLException {

        /* --- DEVICES USED ---
         * Device: Custom Phone
         * Android API: 8.0 - API 26
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: true
         * --------------------
         * Device: Custom Phone
         * Android API: 7.1 - API 25
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: false
         * */

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_PACKAGE + ".Activity.BrowserActivity");
        cap.setCapability("autoLaunch", "false");

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);

    }

    @Test
    public void testRun() {
        WebDriverWait wait = new WebDriverWait(driver, 30);

        long startTime = Instant.now().getEpochSecond();
        // Try to run the application
        try {
            driver.launchApp();
            System.out.println("App launched.");
            TimeUnit.SECONDS.sleep(120); // API 26: 140s f1 - 300s f2 | API 25: 20s f1 - 120s f2
            //wait.until(ExpectedConditions.presenceOfElementLocated(By.id("de.baumann.browser:id/touch_outside")));
            driver.findElementById("de.baumann.browser:id/touch_outside");
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime));
            assert true;
        } catch (Exception e) {
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime));
            assert false;
        }

    }

    @After
    public void tearDown() {
        if (driver.getCapabilities().getCapability("platformVersion").equals("8.0.0"))
            ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_APP_SWITCH);
        else
            driver.quit();
    }

}