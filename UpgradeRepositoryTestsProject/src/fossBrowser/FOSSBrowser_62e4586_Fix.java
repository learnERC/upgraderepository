package fossBrowser;

import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.android.AndroidKeyCode;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FOSSBrowser_62e4586_Fix {

    AppiumDriver driver;

    final String APP_PACKAGE = "de.baumann.browser";

    @Before
    public void setUp() throws MalformedURLException {

        /* --- DEVICES USED ---
         * Device: Custom Phone
         * Android API: 7.1 - API 25
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: true
         * */

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_PACKAGE + ".Activity.BrowserActivity");
        cap.setCapability("autoLaunch", "false");

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);

    }

    @Test
    public void testRun() {

        // Try to run the application
        WebDriverWait wait = new WebDriverWait(driver, 900);

        // Try to run the application
        try {
            driver.launchApp();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("de.baumann.browser:id/touch_outside")));

            if (driver.findElementById("de.baumann.browser:id/touch_outside").isDisplayed())
                assert true;
            else
                // If we are here it means that the app crashed immediately
                assert false;
        } catch (Exception e) {
            assert false;
        }

    }

    @After
    public void tearDown() {
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_APP_SWITCH);
    }

}