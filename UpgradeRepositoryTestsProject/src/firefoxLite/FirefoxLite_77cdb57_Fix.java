package firefoxLite;

import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class FirefoxLite_77cdb57_Fix {

    AppiumDriver driver;

    final String APP_PACKAGE = "org.mozilla.rocket.debug.null";

    WebDriverWait wait;

    /*
     *  IMPORTANT:
     *  1) GIVE AT LEAST 4GB OF RAM TO THE EMULATOR IN ORDER TO RUN PROPERLY THE TEST WITH ANDROID TRACER MODULE ENABLED
     *  2) THE TRACE TO PULL IS THE ONE OF THE PRIVATE MODE BECAUSE THE BUG IS THERE
     *  3) SKIP THE INTRODUCTION OF THE APP ONCE BEFORE RUNNING THE TEST, THUS IN THE TEST IT IS POSSIBLE TO START
     *     DIRECTLY FROM THE PRIVATE MODE
     */

    @Before
    public void setUp() throws MalformedURLException {

        /* --- DEVICES USED ---
         * Device: Custom Phone
         * Android API: 5.1 - API 22
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: true
         * */

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        //cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, "org.mozilla.rocket.privately.PrivateModeActivity");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 960);
        cap.setCapability("noReset", true);

        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);

        wait = new WebDriverWait(driver, 900);

    }

    @Test
    public void testPresenceOfElements() throws InterruptedException {

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(APP_PACKAGE + ":id/tab_view_slot")));

        // Go to a random site while in private mode
        driver.findElementById(APP_PACKAGE + ":id/tab_view_slot").click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(APP_PACKAGE + ":id/url_edit")));

        driver.findElementById(APP_PACKAGE + ":id/url_edit").sendKeys("z");
        ((AndroidDriver) driver).pressKeyCode(66); // enter button

        // Press the shield button
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(APP_PACKAGE + ":id/btn_tracker")));
        driver.findElementById(APP_PACKAGE + ":id/btn_tracker").click();
        Thread.sleep(1000);

        // Go back and check that the popup is no longer displayed
        driver.navigate().back();
        Thread.sleep(1000);

        try {
            driver.findElementById(APP_PACKAGE + ":id/tracker_title").click();
            assert false;
        } catch (Exception e) {
            assert true;
        }

    }

    @After
    public void tearDown() {
        closeRecentApps();
        driver.quit();
    }

    public void closeRecentApps() {

        final String CLOSE_BUTTON_ID = "com.android.systemui:id/dismiss_task";

        /* Open the recent apps menu and close all of them. This must be done
           in order to verify that the tracer stopped correctly */
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_APP_SWITCH);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(CLOSE_BUTTON_ID)));
        boolean flag = driver.findElementById(CLOSE_BUTTON_ID).isDisplayed();
        while(flag) {
            driver.findElementById(CLOSE_BUTTON_ID).click();
            try {
                Thread.sleep(3000);
                driver.findElementById(CLOSE_BUTTON_ID).isDisplayed();
            } catch (Exception e) {
                flag = false;
            }
        }

    }

}