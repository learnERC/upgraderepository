package firefoxLite;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.concurrent.TimeUnit;
import libs.AndroidHelpFunction;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;
import libs.AndroidHelpFunction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import static libs.AndroidHelpFunction.runtimeExec;

public class FirefoxLite_f05fd52_Bug {

    AppiumDriver driver;

    final String APP_PACKAGE = "org.mozilla.rocket.debug.null";
    AndroidHelpFunction androidHelpFunction;
    String tracePath = "/data/data/" + APP_PACKAGE + "/files/";
    String adbPath = "/home/christian/Android/Sdk/platform-tools/adb";

    WebDriverWait wait;

    /*
     *  IMPORTANT:
     *  1) GIVE AT LEAST 4GB OF RAM TO THE EMULATOR IN ORDER TO RUN PROPERLY THE TEST WITH ANDROID TRACER MODULE ENABLED
     *  2) THE TRACE TO PULL IS THE ONE OF THE PRIVATE MODE BECAUSE THE BUG IS THERE
     *  3) SKIP THE INTRODUCTION OF THE APP ONCE BEFORE RUNNING THE TEST, THUS IN THE TEST IT IS POSSIBLE TO START
     *     DIRECTLY FROM THE PRIVATE MODE
     */

    @Before
    public void setUp() throws Exception {

        /* --- DEVICES USED ---
         * Device: Custom Phone
         * Android API: 6.0 - API 23
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: true
         * --------------------
         * Device: Custom Phone
         * Android API: 5.1 - API 22
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: false
         * */

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        //cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, "org.mozilla.rocket.privately.PrivateModeActivity");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 960);
        cap.setCapability("noReset", true);

        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);

        wait = new WebDriverWait(driver, 300);
        /*androidHelpFunction.clearRecentApps();
        driver.closeApp(); //driver doesn't know that the app was closed*/

    }

    @Test
    public void testPresenceOfElements() throws InterruptedException {
        /*runtimeExec(adbPath + " shell rm -r " + tracePath);
        System.out.println("Old traces removed.");
        driver.launchApp();*/
        long startTime = Instant.now().getEpochSecond();
        TimeUnit.SECONDS.sleep(90); // remove comment and set seconds when using UnimibLogger: a1f1=90 - a2f1=90 - a1f2=120 - a2f2=150

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(APP_PACKAGE + ":id/tab_view_slot")));

        // Go to a random site while in private mode
        System.out.println("Go to a random site while in private mode");
        driver.findElementById(APP_PACKAGE + ":id/tab_view_slot").click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(APP_PACKAGE + ":id/url_edit")));

        driver.findElementById(APP_PACKAGE + ":id/url_edit").sendKeys("z");
        ((AndroidDriver) driver).pressKeyCode(66); // enter button

        // Press the shield button
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(APP_PACKAGE + ":id/btn_tracker")));
        driver.findElementById(APP_PACKAGE + ":id/btn_tracker").click();
        Thread.sleep(1000);

        // Go back and check that the popup is no longer displayed
        driver.navigate().back();
        Thread.sleep(1000);

        try {
            driver.findElementById(APP_PACKAGE + ":id/tracker_title").click();
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
            assert false;
        } catch (Exception e) {
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
            assert true;
        }

    }

    @After
    public void tearDown() {
        driver.quit();
    }

}