package filePickerLibrary;

import static org.junit.Assert.assertEquals;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.remote.MobileCapabilityType;
import libs.AndroidHelpFunction;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.List;


public class FilePickerLibrary_213ea56_fix_Test {
    AppiumDriver driver;
    String AppName = "FixFilePickerDemo";
    String APP_PACKAGE = "com.devpaul.filepickerfix";
    String APP_ACTIVITY = "com.devpaul.filepicker.MainActivity";
    AndroidHelpFunction androidHelpFunction;

    @Before
    public void setUp() throws Exception{
        //service.start();
        //reader.readFile();
        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);

        //wait the activity
        cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);
        androidHelpFunction.clearRecentApps();
    }


    @After
    public void tearDown() throws Exception{

        driver.quit();
    }

    @Test
    public void run() throws InterruptedException {

        //return on app
        driver.closeApp(); //driver doesn't know that the app was closed
        driver.launchApp();

        //show bug
        androidHelpFunction.waitForId(APP_PACKAGE + ":id/file_picker_activity");
        driver.findElementById(APP_PACKAGE + ":id/file_picker_activity").click();
        System.out.println("Click on 'ACTIVITY' button.");
        androidHelpFunction.waitForId(APP_PACKAGE + ":id/file_picker_add_button");

        if (androidHelpFunction.textIsPresent("Music")){
            assert true;
        }else{
            assert false;
        }
    }
}