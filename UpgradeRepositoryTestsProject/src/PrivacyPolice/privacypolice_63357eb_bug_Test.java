package privacyPolice;

import libs.AndroidHelpFunction;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static libs.AndroidHelpFunction.getAPILevel;
import static libs.AndroidHelpFunction.runtimeExec;


public class privacypolice_63357eb_bug_Test {
    AppiumDriver driver;
    String AppName = "Bug-Wi-Fi Privacy Police";
    String AppNameFix = "Fix-Wi-Fi Privacy Police";
    String APP_PACKAGE = "be.uhasselt.privacypolicebug";
    String APP_PACKAGE_FIX = "be.uhasselt.privacypolicefix";
    String APP_ACTIVITY = "be.uhasselt.privacypolice.PreferencesActivity";
    AndroidHelpFunction androidHelpFunction;
    String tracePath = "/data/data/" + APP_PACKAGE + "/files/";
    String adbPath = "/home/christian/Android/Sdk/platform-tools/adb";

    @Before
    public void setUp() throws Exception{

        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        //cap.setCapability(MobileCapabilityType.APP,"C:\\apk\\privacypolice-63357eb-bug.apk" );
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);
        ///////////////////
        // SETUP SYSTEM  //
        ///////////////////
        //wait the activity
        cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);

        androidHelpFunction.setTimeOut(360);

        //clear recent apps
        androidHelpFunction.clearRecentApps();
        //reset app
        androidHelpFunction.clearDataApp(AppName, APP_PACKAGE);
        String Str = driver.getCapabilities().getCapability("platformVersion").toString();
//        if (Str.equals("6.0")) {
//            try{
//                //Reset both app because may cause conflicts with the test if active
//                androidHelpFunction.clearDataApp(AppNameFix, APP_PACKAGE_FIX);
//            }catch (Exception e){
//                System.out.println("Error reset app.");
//            }
//        }

        //go to Wi‑Fi settings
        androidHelpFunction.goTowifiSettings();
        //set Wi‑Fi on
        androidHelpFunction.set_WiFi_switch_widget(true);
        //set Location on
        androidHelpFunction.go_to_Location_and_set_switch_widget(true);
    }


    @After
    public void tearDown() throws Exception{
        driver.quit();
    }

    @Test
    public void run() throws Exception {
        //return on app
        driver.closeApp(); //driver doesn't know that the app was closed
        runtimeExec(adbPath + " shell rm -r " + tracePath);
        System.out.println("Old traces removed.");
        long startTime = Instant.now().getEpochSecond();
        driver.launchApp();
        androidHelpFunction.waitForText("Data protection");

        //enable "Data protection"
        //get list switch
        List<WebElement> li = driver.findElementsByClassName("android.widget.Switch");
        li.get(1).click();
        //add wifi "WiredSSID" in known hotspot
        TimeUnit.SECONDS.sleep(10);
        System.out.println("Drop down notification bar.");
        Runtime.getRuntime().exec(adbPath + " shell service call statusbar 1");
        androidHelpFunction.waitForText("Yes");
        System.out.println("Click 'Yes'.");
        androidHelpFunction.textIsPresent("Yes");
        li = driver.findElementsByClassName("android.widget.Button");
        li.get(1).click();
        if(androidHelpFunction.idIsPresent("com.android.systemui:id/dismiss_text")){
            //Collapse status bar
            Runtime.getRuntime().exec(adbPath + " shell service call statusbar 2");
        }
        androidHelpFunction.waitForText("Known hotspots");
//        System.out.println("Click 'Known hotspots'.");
//        //driver.findElementByName("Known hotspots").click();
//        driver.findElementByXPath("//*[contains(@text, '" + text + "')]").click();
//        androidHelpFunction.waitForId(APP_PACKAGE + ":id/SSIDname");
//        driver.findElementsByName("WiredSSID");

        //SHOW BUG
        //set Location off
        androidHelpFunction.go_to_Location_and_set_switch_widget(false);
        //go to wifi settings
        androidHelpFunction.goTowifiSettings();
        //turn off, after turn on
        androidHelpFunction.set_WiFi_switch_widget(false);
        androidHelpFunction.waitForText("Off");
        androidHelpFunction.set_WiFi_switch_widget(true);
        androidHelpFunction.waitForText("On");

        //check if is connected
        //driver.findElementByName("WiredSSID").click();
        driver.findElementByXPath("//*[contains(@text, 'WiredSSID')]").click();
        try {
            androidHelpFunction.idIsPresent("android:id/alertTitle");
        }catch (Exception e){
            //sometimes click doesn't open "info" --> click again
            //driver.findElementByName("WiredSSID").click();
            driver.findElementByXPath("//*[contains(@text, 'WiredSSID')]").click();
            androidHelpFunction.waitForId("android:id/alertTitle");
        }

        if (androidHelpFunction.textIsPresent("Connect")){
            //androidHelpFunction.clickOnBACK(); // remove comment when testing on api 22
            androidHelpFunction.waitForId("android:id/button1"); // comment when testing on api 22
            driver.findElementById("android:id/button1").click(); // comment when testing on api 22
        }
        androidHelpFunction.clickOnHOME();
        androidHelpFunction.goTowifiSettings();
        if (androidHelpFunction.textIsPresent("Connected") || (androidHelpFunction.textIsPresent("Connected, no Internet"))){
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            assert true;
        }else{
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            assert false;
        }
/*
        Thread.sleep(3000);
        new File(getAPILevel()).mkdirs();
        runtimeExec(adbPath + " pull " + tracePath + " " + getAPILevel() + File.separator + APP_PACKAGE + File.separator);
*/
    }
}