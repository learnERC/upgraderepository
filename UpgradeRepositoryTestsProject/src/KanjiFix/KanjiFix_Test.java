package kanjiFix;

import libs.AndroidHelpFunction;
import libs.ImageCompare;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import static libs.AndroidHelpFunction.runtimeExec;

public class KanjiFix_Test {
    AppiumDriver driver;

    String AppName = "KanjiFix";
    String APP_PACKAGE = "com.ascendtv.kanjifix";
    String APP_ACTIVITY = "com.ascendtv.kanjifix.MainActivity";
    String tracePath = "/data/data/" + APP_PACKAGE + "/files/";
    String adbPath = "/home/christian/Android/Sdk/platform-tools/adb";
    String imageFolder = "/home/christian/Documenti/KanjiFix/"; // percorso in cui salvare/leggere le immagini
    AndroidHelpFunction androidHelpFunction;

    /*
     *  IMPORTANT:
     *  - BEFORE RUNNING THE TEST FOR THE FIRST TIME, DO IT MANUALLY TO REMEMBER SUPERUSER PERMISSION
     *  Tested with:
     *  - Screen Size: 1080px x 1920px
     *  - Screen Density: 480dpi - XXHDPI
     */

    @Before
    public void setUp() throws Exception{
        //service.start();
        //reader.readFile();
        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);

        //wait the activity
        cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);
        androidHelpFunction.clearRecentApps();
    }


    @After
    public void tearDown() throws Exception{

        driver.quit();
    }

    @Test
    public void run() throws InterruptedException {
        //return on app
        driver.closeApp(); //driver doesn't know that the app was closed
        runtimeExec(adbPath + " shell rm -r " + tracePath);
        System.out.println("Old traces removed.");
        long startTime = Instant.now().getEpochSecond();
        driver.launchApp();

        //show bug
        androidHelpFunction.waitForId(APP_PACKAGE + ":id/testFont");

        WebElement bottone = driver.findElement(By.id(APP_PACKAGE + ":id/apply_fix_btn"));

        if(bottone.isEnabled()){

            bottone.click();
            System.out.println("****************************");
            System.out.println("* RIAVVIARE IL DISPOSITIVO *");
            System.out.println("****************************");
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
            assert true;

        } else {

            // swipe up
            Dimension size = driver.manage().window().getSize(); // dimensione dello schermo
            int X = size.width / 2; // al centro sull'asse X
            int startY = size.height - 200; // parto dal basso sull'asse Y
            int endY = size.height / 2; // arrivo al centro sull'asse Y

            System.out.println("Size of screen: " + size); // dimensione dello schermo
            System.out.println("Swipe from (" + X + ", " + startY + ") to (" + X + ", " + endY + ")"); // coordinate swipe

            driver.swipe(X, startY, X, endY, 3000); // scorro in alto

            String screenshotName = "screenshot.png"; // nome screenshot
            String immagineFix = "immagine_fix.png"; // nome immagine di controllo

            String ID = APP_PACKAGE + ":id/testFont";

            System.out.println("Comparing images...");

            try {
                WebElement ele = driver.findElement(By.id(ID));

                captureElementScreenshot(ele, (imageFolder + screenshotName)); // screenshot dell'elemento

                ImageCompare ic = new ImageCompare((imageFolder + immagineFix), (imageFolder + screenshotName)); // creo oggetto di confronto tra 2 immagini

                ic.setParameters(25, 25, 2, 25); // parametri: (colonne, righe, sensibilità, stabilizer)

                //ic.setDebugMode(2); // visualizza differenze

                ic.compare(); // comparazione

                System.out.println("Match: " + ic.match());

                if (ic.match()) { // se le immagini danno un match
                    System.out.println("...The images are equal");
                    androidHelpFunction.clickOnHOME();
                    System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
                    TimeUnit.SECONDS.sleep(10);
                    assert true;
                } else {
                    System.out.println("...The images are NOT equal");
                    androidHelpFunction.clickOnHOME();
                    System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
                    TimeUnit.SECONDS.sleep(10);
                    assert false;
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
                androidHelpFunction.clickOnHOME();
                System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
                TimeUnit.SECONDS.sleep(10);
                assert false;
            }

        }

        Thread.sleep(1000);
    }

    public void captureElementScreenshot(WebElement element, String output) throws IOException{
        //Capture entire page screenshot as buffer.
        //Used TakesScreenshot, OutputType Interface of selenium and File class of java to capture screenshot of entire page.
        File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        //Used selenium getSize() method to get height and width of element.
        //Retrieve width of element.
        int ImageWidth = element.getSize().getWidth();
        //Retrieve height of element.
        int ImageHeight = element.getSize().getHeight();

        //Used selenium Point class to get x y coordinates of Image element.
        //get location(x y coordinates) of the element.
        Point point = element.getLocation();
        int xcord = point.getX();
        int ycord = point.getY();

        //Reading full image screenshot.
        BufferedImage img = ImageIO.read(screen);

        //cut Image using height, width and x y coordinates parameters.
        BufferedImage dest = img.getSubimage(xcord, ycord, ImageWidth, ImageHeight);
        ImageIO.write(dest, "png", screen);

        //Used FileUtils class of apache.commons.io.
        //save Image screenshot
        FileUtils.copyFile(screen, new File(output));
    }
}
