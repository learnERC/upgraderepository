package mapboxAndroidDemo;

import io.appium.java_client.*;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MapboxAndroidDemo_2339832 {

    /* IMPORTANT
     *
     *  THE APP MUST BE IN THE HOME SCREEN FOR ANDROID PIE TEST BECAUSE IT'S IMPOSSIBLE (AT THE MOMENT) TO OPEN THE MENU
     *  TO VIEW ALL THE EXISTING APPS IN THE DEVICE
     *
     * */

    AppiumDriver driver;
    TouchAction action;

    final String APP_PACKAGE = "com.mapbox.mapboxandroiddemo";

    @Before
    public void setUp() throws MalformedURLException {

        /* --- DEVICES USED ---
         * Device: Custom Phone
         * Android API: 8.0 - API 26
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: true
         * --------------------
         * Device: Google Pixel 2
         * Android API: 9.0 - API 28
         * Size: 1080 x 1920
         * Density: 420
         * Assert expected: false
         * */

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE,  APP_PACKAGE + ".debug");
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_PACKAGE + ".account.LandingActivity");
        // This capability has been added because the app crashes if not launched manually
        cap.setCapability("autoLaunch", false);

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);

    }

    public void scrollDown() {

        Dimension dimension = driver.manage().window().getSize();

        Double scrollHeightStart = dimension.getHeight() * 0.5;
        int scrollStart = scrollHeightStart.intValue();

        Double scrollHeightEnd = dimension.getHeight() * 0.2;
        int scrollEnd = scrollHeightEnd.intValue();

        movePoint(0, scrollStart, 0, scrollEnd);

    }

    public void movePoint(int xStart, int yStart, int xEnd, int yEnd) {

        action = new TouchAction(driver);
        //action.longPress(PointOption.point(xStart, yStart)).moveTo(PointOption.point(xEnd, yEnd)).release().perform();
        driver.swipe(xStart, yStart, xEnd, yEnd, 1000);
    }

    @Test
    public void crashApp() throws InterruptedException {

        final String PATH_LAB = "//android.widget.CheckedTextView[@text='Lab']";
        final String PATH_GIF = "//android.widget.TextView[@text='Show an animated image (GIF) anywhere on the map']";

        boolean osVersionIs8_0 = driver.getCapabilities().getCapability("platformVersion").equals("8.0.0");

        WebDriverWait wait = new WebDriverWait(driver, 30);

        // Run the app manually
        if (osVersionIs8_0)
            driver.findElementByAccessibilityId("Apps list").click();
        driver.findElementByAccessibilityId("Mapbox Demo").click();

        // Skip the login activity
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(APP_PACKAGE + ".debug:id/button_skip_for_now")));

        driver.findElementById(APP_PACKAGE + ".debug:id/button_skip_for_now").click();

        // Go to the "lab menu"
        driver.findElementByAccessibilityId("Open navigation drawer").click();
        Thread.sleep(3000);
        while (driver.findElementsByXPath(PATH_LAB).size() == 0)
            scrollDown();

        if (driver.findElementsByXPath(PATH_LAB).size() > 0) {
            List<MobileElement> els1 = driver.findElementsByXPath(PATH_LAB);
            els1.get(0).click();
        }
        Thread.sleep(5000);

        // Scroll the view and choose the "Add GIF option"
        while (driver.findElementsByXPath(PATH_GIF).size() == 0)
            scrollDown();

        if (driver.findElementsByXPath(PATH_GIF).size() > 0) {
            List<MobileElement> els2 = driver.findElementsByXPath(PATH_GIF);
            els2.get(0).click();
        }

        Thread.sleep(10000);
        // Check if the app crashed
        if (((AndroidDriver) driver).currentActivity().equals(
                "com.mapbox.mapboxandroiddemo.examples.labs.AnimatedImageGifActivity"))
            assert true;
        else
            assert false;

    }

    @After
    public void tearDown() {
        driver.quit();
    }

}