package acrylicPaint;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import libs.AndroidHelpFunction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AcrylicPaint_test {
    AppiumDriver driver;

    String AppName = "AcrylicPaint";
    String APP_PACKAGE = "anupam.acrylic";
    String APP_ACTIVITY = "anupam.acrylic.Splash";

    AndroidHelpFunction androidHelpFunction;

    @Before
    public void setUp() throws Exception{
        //service.start();
        //reader.readFile();
        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);

        //wait the activity
        cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);
        androidHelpFunction.clearRecentApps();
    }


    @After
    public void tearDown() throws Exception{

        driver.quit();
    }

    @Test
    public void run() throws InterruptedException {

        //return on app
        driver.closeApp(); //driver doesn't know that the app was closed
        driver.launchApp();

        androidHelpFunction.waitForTextAndClick("Continue");

        //show bug
        String testo = "Acrylic Paint";
        androidHelpFunction.waitForText(testo); // attendo il testo

        /*if (!androidHelpFunction.classIsPresent("android.widget.ImageView")){
            assert true;
        } else {
            assert false;
        }*/
        androidHelpFunction.clickOnHOME();
        TimeUnit.SECONDS.sleep(10);
        assert false;

    }
}
