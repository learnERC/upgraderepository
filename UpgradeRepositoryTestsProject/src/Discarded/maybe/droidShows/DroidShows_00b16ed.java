package newApps.maybe.droidShows;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class DroidShows_00b16ed {

    AppiumDriver driver;

    final String APP_PACKAGE = "nl.asymmetrics.droidshows";

    @Before
    public void setUp() throws MalformedURLException {

        /* --- DEVICES USED ---
         * Device: Samsung Galaxy S9
         * Android API: 8.0 - API 26
         * Size: 1440 x 2960
         * Density: 560
         * Assert expected: true
         * --------------------
         * Device: Google Pixel 2
         * Android API: 9.0 - API 28
         * Size: 1080 x 1920
         * Density: 420
         * Assert expected: false
         * */

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_PACKAGE + ".DroidShows");

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);

    }

    @Test
    public void testMainMenu() throws InterruptedException {

        // Wait for the activity to load
        Thread.sleep(3000);

        // If the main menu is not displayed (because of the presence of the popup) set the variable to false
        try {
            driver.findElementById(APP_PACKAGE + ":id/main").isDisplayed();
            assert true;
        } catch (Exception e) {
            assert false;
        }

    }

    @After
    public void tearDown() {
        driver.quit();
    }

}