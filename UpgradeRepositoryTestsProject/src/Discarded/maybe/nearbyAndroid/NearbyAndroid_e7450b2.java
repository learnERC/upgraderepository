package newApps.maybe.nearbyAndroid;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

public class NearbyAndroid_e7450b2 {

    /*
     *
     *   MUST ACTIVATE GPS IN THE GENYMOTION DEVICE BEFORE RUNNING THE TEST
     *
     */

    AppiumDriver driver;

    final String APP_PACKAGE = "com.esri.android.nearbyplaces";

    @Before
    public void setUp() throws MalformedURLException {

        /* --- DEVICES USED ---
         * Device: Google Nexus 6P
         * Android API: 6.0 - API 23
         * Size: 1440 x 2560
         * Density: 560
         * Assert expected: false
         * --------------------
         * Device: Samsung Galaxy S6
         * Android API: 5.1 - API 22
         * Size: 1080 x 1920
         * Density: 480 - XXHDPI
         * Assert expected: true
         * */

        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_PACKAGE + ".places.PlacesActivity");

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);

    }

    @Test
    public void testPermissions() {

        final String XPATH;

        boolean osVersionIs5_1 = driver.getCapabilities().getCapability("platformVersion").equals("5.1");

        WebDriverWait wait = new WebDriverWait(driver, 30);

        if (osVersionIs5_1)
            XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
                    "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                    "android.view.View/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/" +
                    "android.widget.RelativeLayout[1]";
        else
            XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
                    "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                    "android.view.ViewGroup/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/" +
                    "android.widget.RelativeLayout[1]";

        // Launch the app
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH)));

            /* If there are elements displayed, it means that the app has the necessary permissions given.
            Nevertheless, if the OS version is >= 6.0, permissions must be asked before and the user mus
            allow them. */
            driver.findElementByXPath(XPATH);
            if (osVersionIs5_1)
                assert true;
            else
                assert false;
        } catch (Exception e) {
            assert false;
        }

    }

    @After
    public void tearDown() {
        driver.quit();
    }

}