package huEduroam;

import libs.AndroidHelpFunction;
import libs.ImageCompare;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class hu_eduroam_test {
    AppiumDriver driver;

    String AppName = "hu_eduroam";
    String APP_PACKAGE = "de.hu_berlin.eduroam";
    String APP_ACTIVITY = "de.hu_berlin.eduroam.WiFiEduroam";
    String imageFolder = "/home/christian/Documenti/HU_Eduroam/"; // percorso in cui salvare/leggere le immagini

    AndroidHelpFunction androidHelpFunction;

    @Before
    public void setUp() throws Exception{
        //service.start();
        //reader.readFile();
        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);

        //wait the activity
        cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);
        androidHelpFunction.clearRecentApps();
    }


    @After
    public void tearDown() throws Exception{

        driver.quit();
    }

    @Test
    public void run() throws InterruptedException {

        //return on app
        driver.closeApp(); //driver doesn't know that the app was closed
        driver.launchApp();

        //show bug
        androidHelpFunction.waitForId(APP_PACKAGE + ":id/username_hint"); // attendo che compaia la scritta

        //String testo = "*Angehörige der Informatik, Mathematik und Physik müssen Ihre Domain mit angeben.";
        //String testo = "*Members of the institutes of Computer Science, Mathematics or Physics have to append their domain.";

        String screenshotName = "screenshot.png"; // nome screenshot
        String immagineFix1 = "immagine_fix1.png"; // nome immagine di controllo android 5.0
        String immagineFix2 = "immagine_fix2.png"; // nome immagine di controllo android 4.4

        String ID = APP_PACKAGE + ":id/username_hint";

        System.out.println("Comparing images...");

        try {
            WebElement ele = driver.findElement(By.id(ID));

            captureElementScreenshot(ele, (imageFolder + screenshotName)); // screenshot dell'elemento

            // se le dimensioni sono diverse lancia eccezione
            ImageCompare ic1 = new ImageCompare((imageFolder + immagineFix1), (imageFolder + screenshotName)); // creo oggetto di confronto tra 2 immagini
            try{

                ic1.setParameters(25, 25, 2, 25); // parametri: (colonne, righe, sensibilità, stabilizer)

                //ic1.setDebugMode(2); // visualizza differenze

                ic1.compare(); // comparazione

                System.out.println("Match 1: " + ic1.match());

            } catch(Exception e) {
                System.out.println(e.getMessage());
            }

            // se le dimensioni sono diverse lancia eccezione
            ImageCompare ic2 = new ImageCompare((imageFolder + screenshotName), (imageFolder + immagineFix2)); // creo oggetto di confronto tra 2 immagini
            try{

                ic2.setParameters(25, 25, 2, 25); // parametri: (colonne, righe, sensibilità, stabilizer)

                //ic2.setDebugMode(2); // visualizza differenze

                ic2.compare(); // comparazione

                System.out.println("Match 2: " + ic2.match());

            } catch(Exception e) {
                System.out.println(e.getMessage());
            }

            if (ic1.match() || ic2.match()) { // se le immagini danno un match
                System.out.println("...The images are equal");
                androidHelpFunction.clickOnHOME();
                TimeUnit.SECONDS.sleep(10);
                assert true;
            } else {
                System.out.println("...The images are NOT equal");
                androidHelpFunction.clickOnHOME();
                TimeUnit.SECONDS.sleep(10);
                assert false;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            androidHelpFunction.clickOnHOME();
            TimeUnit.SECONDS.sleep(10);
            assert false;
        }

        Thread.sleep(1000);

    }

    public void captureElementScreenshot(WebElement element, String output) throws IOException {
        //Capture entire page screenshot as buffer.
        //Used TakesScreenshot, OutputType Interface of selenium and File class of java to capture screenshot of entire page.
        File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        //Used selenium getSize() method to get height and width of element.
        //Retrieve width of element.
        int ImageWidth = element.getSize().getWidth();
        //Retrieve height of element.
        int ImageHeight = element.getSize().getHeight();

        //Used selenium Point class to get x y coordinates of Image element.
        //get location(x y coordinates) of the element.
        Point point = element.getLocation();
        int xcord = point.getX();
        int ycord = point.getY();

        //Reading full image screenshot.
        BufferedImage img = ImageIO.read(screen);

        //cut Image using height, width and x y coordinates parameters.
        BufferedImage dest = img.getSubimage(xcord, ycord, ImageWidth, ImageHeight);
        ImageIO.write(dest, "png", screen);

        //Used FileUtils class of apache.commons.io.
        //save Image screenshot
        FileUtils.copyFile(screen, new File(output));
    }
}
