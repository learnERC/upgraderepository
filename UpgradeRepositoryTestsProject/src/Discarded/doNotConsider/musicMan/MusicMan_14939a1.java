package newApps.doNotConsider.musicMan;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class MusicMan_14939a1 {

    AppiumDriver driver;

    final String APP_PACKAGE = "e.planet.musicman";

    @Before
    public void setUp() throws MalformedURLException {

        /* --- DEVICES USED ---
         * Device: Samsung Galaxy S8
         * Android API: 7.0 - API 24
         * Size: 1440 x 2960
         * Density: 640 - XXXHDPI
         * Assert expected: true
         * --------------------
         * Device: Google Nexus 6P
         * Android API: 6.0 - API 23
         * Size: 1440 x 2560
         * Density: 560
         * Assert expected: true
         * */

        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
        cap.setCapability("autoLaunch", false);

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);

    }

    @Test
    public void appInstallation() {

        // Check if the installation of the app has been successful or not
        try {
            driver.installApp("C:/Users/zucca/OneDrive/Desktop/Uni/Stage/FiloApps/MusicMan/BUGGED/Apk/" +
                    "MusicMan-14939a1.apk");
            assert true;
        } catch (Exception e) {
            assert false;
        }

    }

    @After
    public void uninstallApp() {

        if (driver.isAppInstalled(APP_PACKAGE))
            driver.removeApp(APP_PACKAGE);

    }

    @After
    public void tearDown() {
        driver.quit();
    }

}