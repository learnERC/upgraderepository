package newApps.doNotConsider.androidTerminalEmulator;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class AndroidTerminalEmulator_d66e636 {

    AppiumDriver driver;

    final String APP_PACKAGE = "com.offsec.nhterm";

    @Before
    public void setUp() throws MalformedURLException {

        /* --- DEVICES USED ---
         * Device: Samsung Galaxy S9
         * Android API: 8.0 - API 26
         * Size: 1440 x 2960
         * Density: 560
         * Assert expected: true
         * --------------------
         * Device: Samsung Galaxy S7
         * Android API: 7.1 - API 25
         * Size: 1440 x 2560
         * Density: 640 - XXXHDPI
         * Assert expected: false
         * */

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
        cap.setCapability("autoLaunch", false);

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);

    }

    @Test
    public void appInstallation() {

        // Check if the installation of the app has been successful or not
        try {
            // Insert the path of the app "AndroidTerminalEmulator-d66e636.apk"
            driver.installApp("/upgraderepository/Apps/NEW/AndroidTerminalEmulator/" +
                    "AndroidTerminalEmulator-d66e636_(BUG)/Apk/AndroidTerminalEmulator-d66e636.apk");
            assert true;
        } catch (Exception e) {
            assert false;
        }

    }

    @After
    public void uninstallApp() {

        if (driver.isAppInstalled(APP_PACKAGE))
            driver.removeApp(APP_PACKAGE);

        driver.quit();

    }

}