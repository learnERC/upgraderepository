package userLAnd;

import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.android.AndroidKeyCode;
import libs.AndroidHelpFunction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class UserLAnd_1620ec6 {

    AppiumDriver driver;

    final String APP_PACKAGE = "tech.ula";
    AndroidHelpFunction androidHelpFunction;

    @Before
    public void setUp() throws MalformedURLException {

        /* --- DEVICES USED ---
         * Device: Custom Phone
         * Android API: 8.0 - API 26
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: true
         * --------------------
         * Device: Custom Phone
         * Android API: 7.1 - API 25
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: false
         * */

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_PACKAGE + ".MainActivity");

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);

    }

    @Test
    public void testAppNotCrashing() throws InterruptedException {

        final String PATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                "android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/" +
                "android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup[5]";
        final String CONFIRM_BUTTON_ID = "android:id/button1";
        final String PERMISSION_BUTTON_ID = "com.android.packageinstaller:id/permission_allow_button";

        WebDriverWait wait = new WebDriverWait(driver, 60);
        //Thread.sleep(10000);
/*
        boolean found = false;
        for(int i=0; i<4 && found == false; i++) {
            Thread.sleep(50000);
            // Wait for all possible element to appear on the screen
            try {*/
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(PATH)));
                /*found = true;
            } catch (Exception e) {
                found = false;
            }
        }*/

        // Choose Debian service and allow permissions
        androidHelpFunction.waitForTextAndClick("Debian");
        //driver.findElementByXPath(PATH).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(CONFIRM_BUTTON_ID)));
        driver.findElementById(CONFIRM_BUTTON_ID).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(PERMISSION_BUTTON_ID)));
        driver.findElementById(PERMISSION_BUTTON_ID).click();

        // Insert the following data in order to start the service
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(APP_PACKAGE + ":id/text_input_username")));
        driver.findElementById(APP_PACKAGE + ":id/text_input_username").sendKeys("zucca");

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(APP_PACKAGE + ":id/text_input_password")));
        driver.findElementById(APP_PACKAGE + ":id/text_input_password").sendKeys("Zucca");

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(APP_PACKAGE + ":id/text_input_vnc_password")));
        driver.findElementById(APP_PACKAGE + ":id/text_input_vnc_password").sendKeys("Zuccaa97");

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(CONFIRM_BUTTON_ID)));
        driver.findElementById(CONFIRM_BUTTON_ID).click();

        // Select SSH Connection for the service
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(CONFIRM_BUTTON_ID)));
        driver.findElementById(CONFIRM_BUTTON_ID).click();

        // +- 3.5 minute to download the assets based on my wi-fi network
        System.out.println("Wait to download the assets");
        Thread.sleep(20000);
        /* Put the app in background in order to avoid possible crashes
           caused by Genymotion emulator while downloading the files */
        driver.runAppInBackground(70);
        Thread.sleep(10000);

        // Verify that the app still runs correctly after copying the assets
        // downloading required assets...
        // Setting up filesystem...
        System.out.println("Verify that the app still runs correctly after copying the assets");
        try {
            driver.findElementById(APP_PACKAGE + ":id/layout_progress");//id/request_review_insert_point
            androidHelpFunction.clickOnHOME();
            assert true;
        } catch (Exception e) {
            androidHelpFunction.clickOnHOME();
            assert false;
        }

    }

    @After
    public void tearDown() {
        driver.quit();
    }

}