package openTraining;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidKeyCode;
import libs.AndroidHelpFunction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static libs.AndroidHelpFunction.runtimeExec;

public class OpenTraining_d4c7083_Bug {

    AppiumDriver driver;

    final String APP_PACKAGE = "de.skubware.opentraining";
    AndroidHelpFunction androidHelpFunction;
    String tracePath = "/data/data/" + APP_PACKAGE + "/files/";
    String adbPath = "/home/christian/Android/Sdk/platform-tools/adb";

    WebDriverWait wait;

    /*
     *  IMPORTANT:
     *  - OPEN THE FIRST TIME TO CLEAR CHANGELOG POPUP
     */

    @Before
    public void setUp() throws Exception {

        /* --- DEVICES USED ---
         * Device: Custom Phone
         * Android API: 7.1 - API 25
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: true
         * --------------------
         * Device: Custom Phone
         * Android API: 8.0 - API 26
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: false
         * */

        runtimeExec(adbPath + " shell rm -r " + tracePath);
        System.out.println("Old traces removed.");

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        //cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_PACKAGE + ".activity.MainActivity");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 1260);
        cap.setCapability("noReset", true);

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);

        wait = new WebDriverWait(driver, 300);

    }

    @Test
    public void testRun() throws InterruptedException {
        long startTime = Instant.now().getEpochSecond();

        final String XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/";
        final String XPATH_START = XPATH + "android.widget.RelativeLayout/android.widget.Gallery/" +
                "android.view.ViewGroup[1]/android.widget.TextView";
        final String XPATH_WORKOUT = XPATH + "android.widget.FrameLayout/android.widget.FrameLayout/" +
                "android.widget.ListView/android.widget.RelativeLayout[1]/android.widget.RelativeLayout";

        androidHelpFunction.waitForIdAndClick("android:id/button1");

        // Wait for the application to start and load a new workout
        System.out.println("Wait for the application to load a new workout");
        TimeUnit.SECONDS.sleep(10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_START)));
        driver.findElementByXPath(XPATH_START).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("android:id/button2")));
        driver.findElementById("android:id/button2").click();

        // Select the first workout available
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_WORKOUT)));
        driver.findElementByXPath(XPATH_WORKOUT).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("de.skubware.opentraining:id/imageview")));
        // Using TouchAction command here to select the tick because Appium desktop doesn't recognize the
        // relative button. So, it's important to use the same Genymotion models described above
        (new TouchAction(driver)).tap(675, 754).perform();

        Thread.sleep(5000);
        // Show if the notification appeared properly
        ((AndroidDriver) driver).openNotifications();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("android:id/title")));

        if (driver.findElementById("android:id/title").getText().equals("Recovery phase ...") ||
                driver.findElementById("android:id/title").getText().equals("Recovery phase finished")) {
            // In this case, the application sends successfully the notification to the user, informing that the
            // recovery phase is on or it just finished. It depends on how long the test is.
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            assert true;
        } else if (driver.findElementById("android:id/title").getText().equals("Appium Settings")) {
            // In this case, the application doesn't send the notification and instead a toast appears frequently.
            // The toast always appears for several minutes from now, so I suggest to reboot the device to avoid its
            // appearance.
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            assert false;
        } else {
            // Just to check if something wrong happens here
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            assert false;
        }
    }

    @After
    public void tearDown() throws InterruptedException {
        closeRecentApps();
        driver.quit();
    }

    public void closeRecentApps() throws InterruptedException {

        final String CLOSE_BUTTON_ID = "com.android.systemui:id/dismiss_task";

        /* Open the recent apps menu and close all of them. This must be done
           in order to verify that the tracer stopped correctly */
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_APP_SWITCH);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(CLOSE_BUTTON_ID)));
        boolean flag = driver.findElementById(CLOSE_BUTTON_ID).isDisplayed();
        while(flag) {
            Thread.sleep (5000); // added only for the fix test
            driver.findElementById(CLOSE_BUTTON_ID).click();
            try {
                Thread.sleep(3000);
                driver.findElementById(CLOSE_BUTTON_ID).isDisplayed();
            } catch (Exception e) {
                flag = false;
            }
        }

    }

}