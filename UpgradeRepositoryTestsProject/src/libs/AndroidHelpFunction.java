package libs;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;


public class AndroidHelpFunction {
    AppiumDriver driver;
    Integer TimeOut=60;
    WebElement WebElement;
    Integer ThreadSleep = 2000;
    String adbPath = "/home/christian/Android/Sdk/platform-tools/adb";

    public AndroidHelpFunction(AppiumDriver driver) {
        this.driver = driver;
    }

    public Integer getTimeOut() {
        return TimeOut;
    }

    public void setTimeOut(Integer timeOut) {
        TimeOut = timeOut;
    }

    public void resetTimeOut() {
        TimeOut = 60;
    }

    public void waitForClass (String className){
        WebElement = (new WebDriverWait(driver, TimeOut)).
                until(ExpectedConditions.presenceOfElementLocated(By.className(className)));
    }

    public void waitForText (String text){
        System.out.println("Wait for '" + text + "'.");
        //WebElement = (new WebDriverWait(driver, TimeOut)).
        //        until(ExpectedConditions.presenceOfElementLocated(By.name(text)));
        WebElement = (new WebDriverWait(driver, TimeOut)).
                until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(@text, '" + text + "')]")));
    }

    public void waitForId (String id){
        System.out.println("Wait for '" + id + "'.");
        WebElement = (new WebDriverWait(driver, TimeOut)).
                until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
    }

    public boolean textIsPresent (String text){
        try{
            //driver.findElementByName(text);
            driver.findElementByXPath("//*[contains(@text, '" + text + "')]");
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean idIsPresent (String text){
        try{
            driver.findElementById(text);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean classIsPresent (String text){
        try{
            driver.findElementByClassName(text);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public void swipingHorizontal(String v) throws InterruptedException {
        Dimension size;
        //Get the size of screen.
        size = driver.manage().window().getSize();
        //System.out.println(size);

        //Find swipe start and end point from screen's with and height.
        //Find startx point which is at right side of screen.
        int startx = (int) (size.width * 0.70);
        //Find endx point which is at left side of screen.
        int endx = (int) (size.width * 0.05);
        //Find vertical point where you wants to swipe. It is in middle of screen height.
        int starty = size.height / 2;
        //System.out.println("startx = " + startx + " ,endx = " + endx + " , starty = " + starty);

        if (v.equals("sx")){
            //Swipe from Right to Left.
            driver.swipe(startx, starty, endx, starty, 1000);
            Thread.sleep(ThreadSleep);
        }

        if (v.equals("dx")){
            //Swipe from Left to Right.
            driver.swipe(0, starty, 60, starty, 1000);
            Thread.sleep(ThreadSleep);
        }
    }

    public void swipingVertical(String v) throws InterruptedException {
        Dimension size;
        //Get the size of screen.
        size = driver.manage().window().getSize();
        //System.out.println(size);

        //Find swipe start and end point from screen's with and height.
        //Find starty point which is at bottom side of screen.
        int starty = (int) (size.height * 0.80);
        //Find endy point which is at top side of screen.
        int endy = (int) (size.height * 0.20);
        //Find horizontal point where you wants to swipe. It is in middle of screen width.
        int startx = size.width / 2;
        //System.out.println("starty = " + starty + " ,endy = " + endy + " , startx = " + startx);

        if (v.equals("up")){
            //Swipe from Bottom to Top.
            driver.swipe(startx, starty, startx, endy, 1000);
            Thread.sleep(ThreadSleep);
        }

        if (v.equals("dw")){
            //Swipe from Top to Bottom.
            driver.swipe(startx, endy, startx, starty, 1000);
            Thread.sleep(ThreadSleep);
        }
    }

    public void dragDrop(WebElement ele1, WebElement ele2) {
        //Perform drag and drop operation using TouchAction class.
        //Created object of TouchAction class.
        TouchAction action = new TouchAction((MobileDriver) driver);

        //System.out.println("It Is dragging element.");
        //It will hold tap on 3rd element and move to 6th position and then release tap.
        action.longPress(ele1).moveTo(ele2).release().perform();
        //System.out.println("Element has been droped at destination successfully.");
    }

    public void goToSettings() throws IOException {
        Runtime.getRuntime().exec(adbPath + " shell am start -n com.android.settings/.Settings");
        waitForText("Settings");
    }

    public void goToMenuApps() throws InterruptedException {
        clickOnHOME();
        //get list of app in home
        List<WebElement> liApp = driver.findElementsByClassName("android.widget.TextView");
        //click on menu apps
        System.out.println("Open menu Apps.");
        int i = 0;
        boolean click=false;
        while (!(click)){
            if (liApp.get(i).getAttribute("text").toString().equals("")){
                liApp.get(i).click();
                click=true;
            }
            i++;
        }
        //wait for the app menu is opened. check with name of app.
        waitForText("Browser");
    }

    public void clickIconInMenuApps(String AppName) throws InterruptedException {
        System.out.println("Click on '" + AppName + "'.");
        boolean click=false;
        while (!click){
            try{
                //search and click on AppName
                List<WebElement> liApps = driver.findElementsByClassName("android.widget.TextView");
                int i = 0;
                click=false;
                while (!(click)){
                    if (liApps.get(i).getAttribute("text").toString().equals(AppName)){
                        liApps.get(i).click();
                        click=true;
                    }
                    i++;
                }
            }catch (Exception e){
                //different menu launcher, different swipe
                String Str = driver.getCapabilities().getCapability("platformVersion").toString();
                if (Str.equals("6.0")) {
                    swipingVertical("up");
                }else{
                    swipingHorizontal("sx");
                }
                click=false;
            }
        }
    }

    public void goTowifiSettings() throws IOException, InterruptedException {
        Runtime.getRuntime().exec(adbPath + " shell am start -n com.android.settings/.wifi.WifiSettings");
        waitForId("com.android.settings:id/switch_widget");
        Thread.sleep(ThreadSleep);
    }

    public void goToApp(String AppName) throws InterruptedException{
        goToMenuApps();
        clickIconInMenuApps(AppName);
    }

    public void go_to_Location_and_set_switch_widget (Boolean v) throws IOException, InterruptedException {
        String value=v.toString();
        //click on "Location"
        Runtime.getRuntime().exec(adbPath + " shell am start -n com.android.settings/.Settings\\$LocationSettingsActivity");
        waitForId("com.android.settings:id/switch_widget");
        Thread.sleep(ThreadSleep);

        try {
            WebElement LocationBtn = driver.findElement(
                    By.id("com.android.settings:id/switch_widget"));

            if (value.equals("true")){
                if (LocationBtn.getAttribute("checked").equals(value)) {
                    System.out.println("Location Already ON");
                } else {
                    LocationBtn.click();
                    waitForText("On");
                    System.out.println("Switch Location ON");
                    clickOnBACK();
                }
            }else if (value.equals("false")){
                if (LocationBtn.getAttribute("checked").equals(value)) {
                    System.out.println("Location Already OFF");
                } else {
                    LocationBtn.click();
                    waitForText("Off");
                    System.out.println("Switch Location OFF");
                }
            }
            Thread.sleep(ThreadSleep);
        } catch (Exception e) {
            System.out.println("Error while set Location");
            Thread.sleep(ThreadSleep);
        }
    }

    public void switch_widget (Boolean b, String switchID) throws  InterruptedException{

        waitForId(switchID);
        Thread.sleep(ThreadSleep);
        String value=b.toString();
        try {
            WebElement btn = driver.findElement(
                    By.id(switchID));

            if (value.equals("true")){
                if (btn.getAttribute("checked").equals(value)) {
                    System.out.println("Switch Already ON");
                } else {
                    btn.click();
                    waitForText("On");
                    System.out.println("Switched ON");
                }
            }else if (value.equals("false")){
                if (btn.getAttribute("checked").equals(value)) {
                    System.out.println("Switch Already OFF");
                } else {
                    btn.click();
                    waitForText("Off");
                    System.out.println("Switched OFF");
                }
            }
            Thread.sleep(ThreadSleep);
        } catch (Exception e) {
            System.out.println("Error while setting Switch");
        }

    }

    public void set_WiFi_switch_widget (Boolean v) throws InterruptedException {
        switch_widget(v, "com.android.settings:id/switch_widget");
    }

    public void disableApp(String AppName) throws IOException {
        goToSettings();
        //click on "Apps"
        System.out.println("Click on 'Apps'.");
        Runtime.getRuntime().exec(adbPath + " shell am start -n com.android.settings/.ManageApplications");

        //wait for the activity is loaded
        String Str = driver.getCapabilities().getCapability("platformVersion").toString();
        if (Str.equals("6.0")) {
            driver.findElementsById("com.android.settings:id/advanced");
        }else{
            waitForText("Downloaded");
        }
        //click on app to reset
        System.out.println("Click on '"+ AppName +"'.");
        driver.scrollTo(AppName);
        waitForId("com.android.settings:id/right_button");
        //driver.findElementByName("Force stop").click();
        driver.findElementByXPath("//*[contains(@text, 'Force stop')]").click();
        try {
            waitForText("Force stop?");
            driver.findElementById("android:id/button1").click();
            System.out.println("App disabled.");
        }catch (Exception e){
            System.out.println("App already disabled.");
        }
        clickOnHOME();
    }

    public void clearDataApp (String AppName, String APP_PACKAGE) throws IOException {
        String Str = driver.getCapabilities().getCapability("platformVersion").toString();
        if (Str.equals("4.2.2")){
            clearDataApp_SDK17(AppName, APP_PACKAGE);
            return;
        }
        //click on "Apps"
        System.out.println("Go to info App.");

        //click on app to reset
        System.out.println("Got to '"+ AppName +"' settings.");
        Runtime.getRuntime().exec(adbPath + " shell am start -a android.settings.APPLICATION_DETAILS_SETTINGS package:" + APP_PACKAGE);
        waitForId("com.android.settings:id/right_button");
        System.out.println("Clear data");
        //different menu settings, different action
        if (Str.equals("6.0") || Str.equals("7.0")) {
            //driver.findElementByName("Storage").click();
            driver.findElementByXPath("//*[contains(@text, 'Storage')]").click();
        }
        try{
            //driver.findElementByName("Clear data").click();
            waitForText("Clear data");
            driver.findElementByXPath("//*[contains(@text, 'Clear data')]").click();
            waitForId("android:id/button1");
            driver.findElementById("android:id/button1").click();
        }catch (Exception e){
            System.out.println("App already reset.");
        }

        //different menu settings, different action
        if (Str.equals("6.0")) {
            waitForText("0.00 B");
        }else{
            waitForText("0.00B");
        }
        clickOnHOME();
    }

    private void clearDataApp_SDK17 (String AppName, String APP_PACKAGE) throws IOException{
        //click on "Apps"
        System.out.println("Go to info App.");

        //click on app to reset
        System.out.println("Got to '"+ AppName +"' settings.");
        Runtime.getRuntime().exec(adbPath + " shell am start -a android.settings.APPLICATION_DETAILS_SETTINGS package:" + APP_PACKAGE);
        waitForText("Clear data");
        System.out.println("Clear data");
        //different menu settings, different action
        try{
            //driver.findElementByName("Clear data").click();
            driver.findElementByXPath("//*[contains(@text, 'Clear data')]").click();
            waitForText("Delete app data?");
            //driver.findElementByName("OK").click();
            driver.findElementByXPath("//*[contains(@text, 'OK')]").click();
        }catch (Exception e){
            System.out.println("App already reset.");
        }

        waitForText("0.00B");
        clickOnHOME();
    }

    public void clearRecentApps() throws InterruptedException {
        String Str = driver.getCapabilities().getCapability("platformVersion").toString();
        if (Str.equals("4.2.2")){
            System.out.println("I can't clear recent apps in SDK 17.");
            return;
        }
        System.out.println("Close all recent apps.");
        clickOnHOME();
        clickOnRECENT_APPS();

        //do different action if run on Android 4.4
        if (Str.equals("4.4.4")) {
            //Android 4.4.4
            try {
                while (true){
                    driver.findElementById("com.android.systemui:id/recent_item");
                    Runtime.getRuntime().exec(adbPath + " shell input touchscreen swipe 700 1620 250 1620");
                    Thread.sleep(ThreadSleep);
                }
            }catch (Exception e){
                System.out.println("All app are close.");
            }
        }else{
            //Android other
            try{
                while (true){
                    Thread.sleep (ThreadSleep);
                    driver.findElementById("com.android.systemui:id/dismiss_task").click();
                }
            }catch (Exception e){
                System.out.println("All recent apps were closed.");
            }
            Thread.sleep (ThreadSleep);
        }
        clickOnHOME();
    }

    public boolean privateFindAndScroll(String text, String direction, Integer attempts) throws InterruptedException {
        Boolean find=false;
        Integer i = attempts;
        while (!find && i>0){
            //scroll
            swipingVertical(direction);
            if(textIsPresent(text)){
                System.out.println(attempts.toString() + " attempts to find ''.");
                return true;
            }
            i--;
        }
        System.out.println(attempts.toString() + " attempts 'FindAndScroll(text)' fail.");
        return false;
    }

    public void clickOnHOME (){
        //go to home and wait until action is ok
        System.out.println("Click HOME.");
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.HOME);
        //do different action if run on Android 4.4
        String Str = driver.getCapabilities().getCapability("platformVersion").toString();
        if (Str.equals("4.4.4")) {
            //Android 4.4.4
            waitForId("com.android.launcher:id/workspace");
        }else if (Str.equals("4.2.2") || (Str.equals("4.3"))){
            waitForText("Phone");
        }else{
            //Android other
            waitForId("com.android.launcher3:id/active");
        }
    }

    public void clickOnRECENT_APPS (){
        System.out.println("Click RECENT APPS.");
        ((AndroidDriver) driver).pressKeyCode(187);
        //do different action if run on Android 4.4
        String Str = driver.getCapabilities().getCapability("platformVersion").toString();
        if (Str.equals("4.4.4")) {
            //Android 4.4.4
            if(idIsPresent("com.android.systemui:id/recents_no_apps")){
                //no recent apps
                System.out.println("All app are close.");
            }else{
                waitForId("com.android.systemui:id/recent_item");
            }
        }else{
            //Android other
            if(idIsPresent("com.google.android.googlequicksearchbox:id/search_widget_google_logo")){
                waitForId("com.android.systemui:id/task_view_thumbnail");
                waitForId("com.android.systemui:id/dismiss_task");
            }else{
                System.out.println("All app are close.");
            }
        }
    }

    public void sendText(String text) throws IOException, InterruptedException {
        TimeOut = 20;
        System.out.println("Send text: '" + text + "'.");
        Runtime.getRuntime().exec(adbPath + " shell input text '" + text + "'");
        waitForText(text);
        TimeOut = 60;
        Thread.sleep(ThreadSleep); //wait a second because operation is too fast
    }

    public void clickOnBACK (){
        System.out.println("Click BACK.");
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.BACK);
    }

    public void clickOnMENU () throws InterruptedException {
        System.out.println("Click MENU.");
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.MENU);
        Thread.sleep(ThreadSleep);
    }

    public void longClickOnText (String text){
        TouchAction action = new TouchAction(driver);
        //action.longPress(driver.findElementByName(text)).release().perform();
        action.longPress(driver.findElementByXPath("//*[contains(@text, '" + text + "')]")).release().perform();
    }

    public void longClickOnId (String id){
        TouchAction action = new TouchAction(driver);
        action.longPress(driver.findElementById(id)).release().perform();
    }

    public void waitForTextAndClick(String text){
        try{
            System.out.println("Click '" + text + "'");
            waitForText(text);
            //driver.findElementByName(text).click();
            driver.findElementByXPath("//*[contains(@text, '" + text + "')]").click();
        }catch(Exception e){
            System.out.println(e.toString());
            assert false;
        }
    }

    public void waitForIdAndClick (String id){
        try{
            System.out.println("Click '" + id + "'");
            waitForId(id);
            driver.findElementById(id).click();
        }catch(Exception e){
            System.out.println(e.toString());
            assert false;
        }
    }
//<<<<<<< Updated upstream

    public void waitForxPathAndClick (String id){
        try{
            System.out.println("Click '" + id + "'");
            waitForId(id);
            driver.findElementByXPath(id).click();
        }catch(Exception e){
            System.out.println(e.toString());
            assert false;
        }
    }

//=======

    public static String getAPILevel(){
        String s = null;
        String adbExecPath = "/home/christian/Android/Sdk/platform-tools/adb";

        try {
            Process process = Runtime.getRuntime().exec(adbExecPath + " shell grep ro.build.version.sdk= system/build.prop");
            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(process.getInputStream()));

            // read the output from the command
            System.out.println("Here is the standard output of the command:\n");
            s = stdInput.readLine();
            s = s.substring(s.indexOf('=')+1);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static void runtimeExec(String command){
        Process process;
        try {
            process = Runtime.getRuntime().exec(command);
            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(process.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(process.getErrorStream()));
            // read the output from the command
            String s = null;
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }

            // read any errors from the attempted command
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
//>>>>>>> Stashed changes
}
