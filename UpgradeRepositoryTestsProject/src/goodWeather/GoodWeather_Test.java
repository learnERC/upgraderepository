package goodWeather;

import libs.AndroidHelpFunction;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.net.URL;
import java.time.Instant;
import java.util.concurrent.TimeUnit;


public class GoodWeather_Test {
    AppiumDriver driver;
    String AppName = "Bug-Good Weather";
    String APP_PACKAGE = "org.asdtm.goodweatherbug";
    String APP_ACTIVITY = "org.asdtm.goodweather.MainActivity";
    AndroidHelpFunction androidHelpFunction;

    @Before
    public void setUp() throws Exception{
        //service.start();
        //reader.readFile();
        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);
        cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);
        System.out.println("****************************");
        System.out.println("* ENABLE GPS ON GENYMOTION *");
        System.out.println("****************************");
    }


    @After
    public void tearDown() throws Exception{

        driver.quit();
    }

    @Test
    public void run() throws InterruptedException {
        long startTime = Instant.now().getEpochSecond();
        androidHelpFunction.waitForIdAndClick(APP_PACKAGE + ":id/main_menu_detect_location");
        try{
            androidHelpFunction.setTimeOut(15);
            System.out.println("Wait for message 'Find current location'.");
            androidHelpFunction.waitForText("Find current location");
            androidHelpFunction.resetTimeOut();
        }catch (Exception e){
            System.out.println("When there is no problem -message- not appears.");
            androidHelpFunction.resetTimeOut();
        }
        try{
            androidHelpFunction.waitForId(APP_PACKAGE + ":id/main_menu_detect_location");
        } finally {
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
        }
    }
}
