package toneDef;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import libs.AndroidHelpFunction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;


public class ToneDef_test {
    AppiumDriver driver;
    String AppName = "toneDef";
    String APP_PACKAGE = "com.bytestemplar.tonedef";
    String APP_ACTIVITY = "com.bytestemplar.tonedef.MainActivity";
    AndroidHelpFunction androidHelpFunction;
    String tracePath = "/data/data/" + APP_PACKAGE + "/files/";

    /*
     *  IMPORTANT:
     *  - CREATE A CONTACT WITH A PHONE NUMBER BEFORE RUNNING THE TEST
     */

    @Before
    public void setUp() throws Exception {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);

        cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);

        //clear recent apps
        androidHelpFunction.clearRecentApps();
    }


    @After
    public void tearDown() throws Exception{
        driver.quit();
    }
    @Test
    public void run() throws InterruptedException, IOException {
        driver.closeApp();
        long startTime = Instant.now().getEpochSecond();
        driver.launchApp();
        //androidHelpFunction.setTimeOut(240);

        int timeout = androidHelpFunction.getTimeOut();
        try{ //The Known issues window does not appear always

            androidHelpFunction.setTimeOut(30);
            androidHelpFunction.waitForIdAndClick(APP_PACKAGE + ":id/OK");
            androidHelpFunction.setTimeOut(timeout);
        }catch (Exception e){
            androidHelpFunction.setTimeOut(timeout);
        }

        androidHelpFunction.waitForIdAndClick(APP_PACKAGE + ":id/menu_dtmf");
        driver.findElementByAccessibilityId("More options").click();
        //androidHelpFunction.waitForIdAndClick(APP_PACKAGE + ":id/title");
        Thread.sleep(500);
        TouchAction touchAction = new TouchAction(driver);
        touchAction.tap(500,100).perform(); //Dial contact
        //androidHelpFunction.waitForxPathAndClick("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.TextView");
        Thread.sleep(3000);

        System.out.println("Waiting for permissions request");
        try {
            androidHelpFunction.waitForId("com.android.packageinstaller:id/dialog_container");
            System.out.println("Dialog found");
            androidHelpFunction.waitForIdAndClick("com.android.packageinstaller:id/permission_allow_button");
            System.out.println("Allow button found");
        }catch (Exception e){
            System.out.println("No dialog found");
        }

        androidHelpFunction.waitForIdAndClick("com.android.contacts:id/cliv_name_textview");

        //Thread.sleep(3000);
        try {
            androidHelpFunction.waitForId("android:id/button1");
        }catch (Exception e){

        }

        if (androidHelpFunction.idIsPresent("android:id/button1")){
        //if (androidHelpFunction.idIsPresent(APP_PACKAGE + ":id/button1")){
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            assert false;
        } else {
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            assert true;
        }
    }
}