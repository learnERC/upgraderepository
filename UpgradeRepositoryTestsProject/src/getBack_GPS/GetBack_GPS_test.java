package getBack_GPS;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import libs.AndroidHelpFunction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import static libs.AndroidHelpFunction.runtimeExec;


public class GetBack_GPS_test {
    AppiumDriver driver;
    String AppName = "GetBack GPS";
    String APP_PACKAGE = "com.github.ruleant.getback_gps";
    String APP_ACTIVITY = "com.github.ruleant.getback_gps.MainActivity";
    AndroidHelpFunction androidHelpFunction;
    String tracePath = "/data/data/" + APP_PACKAGE + "/files/";
    String adbPath = "/home/christian/Android/Sdk/platform-tools/adb";


    @Before
    public void setUp() throws Exception {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);

        cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);

        //clear recent apps
        androidHelpFunction.clearRecentApps();
    }


    @After
    public void tearDown() throws Exception{
        driver.quit();
    }
    @Test
    public void run() throws InterruptedException, IOException {
        driver.closeApp();
        runtimeExec(adbPath + " shell rm -r " + tracePath);
        System.out.println("Old traces removed.");
        long startTime = Instant.now().getEpochSecond();
        driver.launchApp();

        System.out.println("Waiting for permissions request");
        try {
            androidHelpFunction.waitForId("com.android.packageinstaller:id/dialog_container");
            System.out.println("Dialog found");
            androidHelpFunction.waitForId("com.android.packageinstaller:id/permission_allow_button");
            System.out.println("Allow button found");
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            assert true; //permission request found -> fixed version
            return;
        }catch (Exception e){
            System.out.println("No dialog found");
        }

        androidHelpFunction.waitForId("com.github.ruleant.getback_gps:id/menu_storelocation");
        WebElement button = driver.findElementById("com.github.ruleant.getback_gps:id/menu_storelocation");
        String isEnabled = button.getAttribute("enabled");
        System.out.println("isEnabled: " + isEnabled);

        androidHelpFunction.clickOnHOME();
        TimeUnit.SECONDS.sleep(10);

        if (isEnabled.equalsIgnoreCase("true")){
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            assert true;
        } else {
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            assert false;
        }
    }
}