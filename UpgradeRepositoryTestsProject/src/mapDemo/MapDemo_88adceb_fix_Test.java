package mapDemo;

import libs.AndroidHelpFunction;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.List;


public class MapDemo_88adceb_fix_Test {
    AppiumDriver driver;
    String AppName = "FIX-Map Demo";
    String APP_PACKAGE = "com.example.mapdemofix";
    String APP_ACTIVITY = "com.example.mapdemo.MapDemoActivity";
    libs.AndroidHelpFunction androidHelpFunction;

    @Before
    public void setUp() throws Exception{
        //service.start();
        //reader.readFile();
        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);
        cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);
        System.out.println("****************************");
        System.out.println("* ENABLE GPS ON GENYMOTION *");
        System.out.println("****************************");
        androidHelpFunction.clearRecentApps();

    }


    @After
    public void tearDown() throws Exception{

        driver.quit();
    }

    @Test
    public void run() throws InterruptedException {
        //relaunch app because appium doesn't recognise that it's closed
        driver.closeApp();
        try {
            Runtime.getRuntime().exec("adb shell rm /data/user/0/"+APP_PACKAGE+"/*.trace");
            System.out.println("Old traces removed.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        driver.launchApp();
        try{
            androidHelpFunction.waitForIdAndClick(APP_PACKAGE + ":id/posButton");
            androidHelpFunction.setTimeOut(480);
            androidHelpFunction.waitForId(APP_PACKAGE + ":id/posButton");
            if(androidHelpFunction.textIsPresent("Unfortunately, Bug-Map Demo has stopped.")){
                assert false;
            }
        }catch (Exception e){
            assert false;
        }
    }
}
