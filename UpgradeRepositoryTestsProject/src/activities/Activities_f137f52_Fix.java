package activities;

import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.android.AndroidKeyCode;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activities_f137f52_Fix {

    AppiumDriver driver;

    final String APP_PACKAGE = "com.cunnj.activities";

    WebDriverWait wait;

    @Before
    public void setUp() throws MalformedURLException {

        /* --- DEVICES USED ---
         * Device: Custom Phone
         * Android API: 7.1 - API 25
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: true
         * --------------------
         * Device: Custom Phone
         * Android API: 8.0 - API 26
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: false
         * */

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_PACKAGE + ".MainActivity");
        cap.setCapability("autoLaunch", "false");

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);

        wait = new WebDriverWait(driver, 60);

    }

    @Test
    public void testRun() {

        final String XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/" +
                "android.widget.LinearLayout/android.widget.TextView";

        driver.launchApp();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH)));

        // Try to run the application
        try {
            if (driver.findElementByXPath(XPATH + "[@text='Activities']").isDisplayed())
                assert true;
            else
                assert false;
        } catch (Exception e) {
            assert false;
        }

    }

    @After
    public void tearDown() {
        closeRecentApps();
        driver.quit();
    }

    public void closeRecentApps() {

        final String CLOSE_BUTTON_ID = "com.android.systemui:id/dismiss_task";

        /* Open the recent apps menu and close all of them. This must be done
           in order to verify that the tracer stopped correctly */
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_APP_SWITCH);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(CLOSE_BUTTON_ID)));
        boolean flag = driver.findElementById(CLOSE_BUTTON_ID).isDisplayed();
        while(flag) {
            driver.findElementById(CLOSE_BUTTON_ID).click();
            try {
                Thread.sleep(3000);
                driver.findElementById(CLOSE_BUTTON_ID).isDisplayed();
            } catch (Exception e) {
                flag = false;
            }
        }

    }

}