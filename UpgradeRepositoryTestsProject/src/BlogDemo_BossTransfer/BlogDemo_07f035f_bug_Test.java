package blogDemo_BossTransfer;

import libs.AndroidHelpFunction;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.net.URL;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import static libs.AndroidHelpFunction.runtimeExec;


public class BlogDemo_07f035f_bug_Test {
    AppiumDriver driver;
    String AppName = "Bug-BossTransfer";
    String APP_PACKAGE = "com.example.arial.bosstransferbug";
    String APP_ACTIVITY = "com.example.arial.bosstransfer.MainActivity";
    libs.AndroidHelpFunction androidHelpFunction;
    String tracePath = "/data/data/" + APP_PACKAGE + "/files/";
    String adbPath = "/home/christian/Android/Sdk/platform-tools/adb";

    @Before
    public void setUp() throws Exception{
        //service.start();
        //reader.readFile();
        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);
        //cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);
        androidHelpFunction.clearRecentApps();
        //return on app
        driver.closeApp(); //driver doesn't know that the app was closed
        runtimeExec(adbPath + " shell rm -r " + tracePath);
        System.out.println("Old traces removed.");
    }


    @After
    public void tearDown() throws Exception{

        driver.quit();
    }

    @Test
    public void run() throws InterruptedException {
        long startTime = Instant.now().getEpochSecond();
        driver.launchApp();

        androidHelpFunction.waitForTextAndClick("item = 1");
        TimeUnit.SECONDS.sleep(5); // comment this line when using android tracer enabler
        /*try {
            androidHelpFunction.setTimeOut(5);
            androidHelpFunction.waitForText("Unfortunately, Bug-BossTransfer has stopped.");
            androidHelpFunction.resetTimeOut();
            assert false;
        }catch (Exception e){
            androidHelpFunction.resetTimeOut();
        }*/

        if (androidHelpFunction.textIsPresent("Unfortunately, " + AppName + " has stopped.")){
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
            assert false;
        }else{
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
            assert true;
        }
    }
}
