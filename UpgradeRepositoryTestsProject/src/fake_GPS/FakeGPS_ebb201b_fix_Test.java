package fake_GPS;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.remote.MobileCapabilityType;
import libs.AndroidHelpFunction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static libs.AndroidHelpFunction.getAPILevel;
import static libs.AndroidHelpFunction.runtimeExec;


public class FakeGPS_ebb201b_fix_Test {
    AppiumDriver driver;
    String AppName = "Fix-FakeGPS";
    String APP_PACKAGE = "com.github.fakegpsfix";
    String APP_ACTIVITY = "com.github.fakegps.ui.MainActivity";
    String tracePath = "/data/data/" + APP_PACKAGE + "/files/";
    String adbPath = "/home/christian/Android/Sdk/platform-tools/adb";
    AndroidHelpFunction androidHelpFunction;
//    Process p;

    @Before
    public void setUp() throws Exception{
        //service.start();
        //reader.readFile();
        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);

        androidHelpFunction = new AndroidHelpFunction(driver);
        androidHelpFunction.clearRecentApps();

        cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);
    }


    @After
    public void tearDown() throws Exception{
        driver.quit();
    }

    @Test
    public void run() throws InterruptedException {

        driver.closeApp();
        runtimeExec(adbPath + " shell rm -r " + tracePath);
        System.out.println("Old traces removed.");
        driver.launchApp();

        try{
            androidHelpFunction.waitForIdAndClick(APP_PACKAGE + ":id/btn_start");
            Runtime.getRuntime().exec(adbPath + " shell am start -n " + APP_PACKAGE + "/" + APP_ACTIVITY);
            androidHelpFunction.setTimeOut(10);
            androidHelpFunction.waitForTextAndClick("Stop");
            assert true;
        }catch (Exception e){
            assert false;
        }

        Thread.sleep(3000);
        new File(getAPILevel()).mkdirs();
        runtimeExec(adbPath + " pull " + tracePath + " " + getAPILevel() + File.separator + APP_PACKAGE + File.separator);

    }
}
