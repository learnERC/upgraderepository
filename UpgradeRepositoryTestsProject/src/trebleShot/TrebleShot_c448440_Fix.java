package trebleShot;

import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.android.AndroidKeyCode;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class TrebleShot_c448440_Fix {

    AppiumDriver driver;

    WebDriverWait wait;

    /*
     *  IMPORTANT:
     *  1) GIVE AT LEAST 4GB OF RAM TO THE EMULATOR IN ORDER TO RUN PROPERLY THE TEST WITH ANDROID TRACER MODULE ENABLED
     *  2) SKIP THE INTRODUCTION OF THE APP ONCE BEFORE RUNNING THE TEST, THUS IN THE TEST IT IS POSSIBLE TO START
     *     DIRECTLY FROM THE HOME ACTIVITY
     */

    @Before
    public void setUp() throws MalformedURLException {

        /* --- DEVICES USED ---
         * Device: Custom Phone
         * Android API: 5.1 - API 22
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: true
         * */

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, "com.genonbeta.TrebleShot.debug");
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, "com.genonbeta.TrebleShot.activity.HomeActivity");
        cap.setCapability("autoGrantPermissions", true);
        cap.setCapability("noReset", true);

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);

        wait = new WebDriverWait(driver, 60);

    }

    @Test
    public void testOptionAvailable() {

        final String XPATH_EXPLORER = "//android.widget.FrameLayout[@content-desc=\"File explorer\"]";
        final String XPATH_OPTIONS = "//android.widget.ImageView[@content-desc=\"More options\"]";
        final String XPATH_FOLDER = "//android.widget.TextView[@text=\"Mount folder\"]";

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_EXPLORER)));

        // Select "File explorer" menu
        driver.findElementByXPath(XPATH_EXPLORER).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_OPTIONS)));

        // Select "More options" (three dots)
        driver.findElementByXPath(XPATH_OPTIONS).click();

        // Check if "Mount folder" option is available
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_FOLDER)));

            driver.findElementByXPath(XPATH_FOLDER).click();
            Thread.sleep(3000);
            assert true;
        } catch (Exception e) {
            assert false;
        }

    }

    @After
    public void tearDown() {
        // In order to create properly the trace - driver.quit() won't work -
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_APP_SWITCH);
    }

}