package trebleShot;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidKeyCode;
import libs.AndroidHelpFunction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import static libs.AndroidHelpFunction.runtimeExec;

public class TrebleShot_bea5ce3_Bug {

    AppiumDriver driver;

    WebDriverWait wait;

    String AppName = "TShot DEBUG";
    String APP_PACKAGE = "com.genonbeta.TrebleShot.debug";
    String APP_ACTIVITY = "com.genonbeta.TrebleShot.activity.HomeActivity";
    AndroidHelpFunction androidHelpFunction;
    String tracePath = "/data/data/" + APP_PACKAGE + "/files/";
    String adbPath = "/home/christian/Android/Sdk/platform-tools/adb";

    /*
     *  IMPORTANT:
     *  1) GIVE AT LEAST 4GB OF RAM TO THE EMULATOR IN ORDER TO RUN PROPERLY THE TEST WITH ANDROID TRACER MODULE ENABLED
     *  2) SKIP THE INTRODUCTION AND GRANT THE PERMISSIONS (ON ANDROID 6.0) OF THE APP ONCE BEFORE RUNNING THE TEST,
     *     THUS IN THE TEST IT IS POSSIBLE TO START DIRECTLY FROM THE HOME ACTIVITY
     */

    @Before
    public void setUp() throws Exception {

        /* --- DEVICES USED ---
         * Device: Custom Phone
         * Android API: 6.0 - API 23
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: true
         * --------------------
         * Device: Custom Phone
         * Android API: 5.1 - API 22
         * Size: 768 x 1280
         * Density: 320 - XHDPI
         * Assert expected: false
         * */

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("platformName", "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Test Device");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        cap.setCapability("autoGrantPermissions", true);
        cap.setCapability("noReset", true);

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);

        wait = new WebDriverWait(driver, 60);
        androidHelpFunction.clearRecentApps();
        driver.closeApp(); //driver doesn't know that the app was closed

    }

    @Test
    public void testOptionAvailable() throws InterruptedException {

        final String XPATH_EXPLORER = "//android.widget.FrameLayout[@content-desc=\"File explorer\"]";
        final String XPATH_OPTIONS = "//android.widget.ImageView[@content-desc=\"More options\"]";
        final String XPATH_FOLDER = "//android.widget.TextView[@text=\"Mount folder\"]";

        TimeUnit.SECONDS.sleep(5);
        runtimeExec(adbPath + " shell rm -r " + tracePath);
        System.out.println("Old traces removed.");
        TimeUnit.SECONDS.sleep(5);
        long startTime = Instant.now().getEpochSecond();
        driver.launchApp();
        //TimeUnit.SECONDS.sleep(150); // remove comment and set seconds when using UnimibLogger: a1f1=150 - a2f1=180 - a1f2=210 - a2f2=210

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_EXPLORER)));

        // Select "File explorer" menu
        driver.findElementByXPath(XPATH_EXPLORER).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_OPTIONS)));

        // Select "More options" (three dots)
        driver.findElementByXPath(XPATH_OPTIONS).click();

        // Check if "Mount folder" option is available
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_FOLDER)));

            driver.findElementByXPath(XPATH_FOLDER).click();
            Thread.sleep(3000);
            assert true;
        } catch (Exception e) {
            assert false;newApps
        } finally {
            androidHelpFunction.clickOnHOME();
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            TimeUnit.SECONDS.sleep(10);
        }

    }

    @After
    public void tearDown() {
        // In order to create properly the trace - driver.quit() won't work -
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_APP_SWITCH);
    }

}