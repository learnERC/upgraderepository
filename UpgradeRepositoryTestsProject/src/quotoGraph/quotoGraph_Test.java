package quotoGraph;

import libs.AndroidHelpFunction;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.net.URL;
import java.time.Instant;
import java.util.List;


public class quotoGraph_Test {
    AppiumDriver driver;
    String AppName = "Bug-Quotograph";
    String APP_PACKAGE = "com.stanleyidesis.quotograph";
    String APP_ACTIVITY = "com.stanleyidesis.quotograph.ui.activity.LWQActivateActivity";
    libs.AndroidHelpFunction androidHelpFunction;

    @Before
    public void setUp() throws Exception{
        //service.start();
        //reader.readFile();
        DesiredCapabilities cap=new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "000e04575a5bcf");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "4000");
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, APP_PACKAGE);
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY);
        driver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        androidHelpFunction = new AndroidHelpFunction(driver);
        //cap.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);
        androidHelpFunction.clearRecentApps();
        androidHelpFunction.clearDataApp(AppName, APP_PACKAGE);
    }


    @After
    public void tearDown() throws Exception{

        driver.quit();
    }

    @Test
    public void run() throws InterruptedException {

        long startTime = Instant.now().getEpochSecond();
        //relaunch app
        androidHelpFunction.goToApp(AppName);

        String Str = driver.getCapabilities().getCapability("platformVersion").toString();
        //If the test is run on android 6.0 marshmallow, the test consent to the use of certain privileges
        if (Str.equals("6.0")) {
        }

        if (androidHelpFunction.idIsPresent("com.stanleyidesis.quotograph:id/lwq_activate_tut_0")){
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            assert true;
        }else{
            System.out.println("Total time: " + (Instant.now().getEpochSecond() - startTime) + " seconds");
            assert false;
        }
    }
}
