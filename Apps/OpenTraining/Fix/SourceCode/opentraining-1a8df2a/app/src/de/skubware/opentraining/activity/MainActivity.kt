/**
 *
 * This is OpenTraining, an Android application for planning your your fitness training.
 * Copyright (C) 2012-2014 Christian Skubich
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.skubware.opentraining.activity

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import at.technikum.mti.fancycoverflow.FancyCoverFlow
import de.skubware.opentraining.R
import de.skubware.opentraining.activity.create_workout.ExerciseTypeListActivity
import de.skubware.opentraining.activity.manage_workouts.WorkoutListActivity
import de.skubware.opentraining.activity.settings.SettingsActivity
import de.skubware.opentraining.activity.start_training.RecoveryTimerManager
import de.skubware.opentraining.db.Cache
import de.skubware.opentraining.db.DataProvider
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {
    /** Tag for logging */
    val TAG = MainActivity::class.java.name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createNotificationChannel()
        setContentView(R.layout.activity_navigation_layout)
        supportActionBar!!.setIcon(R.drawable.icon_dumbbell)
        setUpNavigation()
        // load data/parse .xml files in background
        thread(start = true) {
            Cache.INSTANCE.updateCache(applicationContext)
        }
        //Launch what's new dialog
        val whatsNewDialog = WhatsNewDialog(this)
        whatsNewDialog.show() // (will only be shown if started the first time since last change)
        // show disclaimer
        val settings = PreferenceManager.getDefaultSharedPreferences(this)
        val showDisclaimer = settings.getBoolean(DisclaimerDialog.PREFERENCE_SHOW_DISCLAIMER, true)
        if (showDisclaimer) {
            DisclaimerDialog(this)
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Recovery timer"
            val descriptionText = "Recovery timer notification channel"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(RecoveryTimerManager.RECOVERY_CHANNEL_ID, name, importance)
                    .apply {
                        description = descriptionText
                    }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun setUpNavigation() {
        val mFancyCoverFlow = findViewById<FancyCoverFlow>(R.id.fancyCoverFlow)
        mFancyCoverFlow.adapter = NavigationGalleryAdapter(applicationContext)
        mFancyCoverFlow.unselectedAlpha = 0.5f
        mFancyCoverFlow.unselectedSaturation = 0.0f
        mFancyCoverFlow.unselectedScale = 0.2f
        mFancyCoverFlow.maxRotation = 35
        mFancyCoverFlow.scaleDownGravity = 0.0f
        mFancyCoverFlow.actionDistance = FancyCoverFlow.ACTION_DISTANCE_AUTO

        mFancyCoverFlow.setOnItemClickListener { _, _, position, _ ->
            when (position) {
                0 -> showSelectWorkoutDialog()
                1 -> startActivity(Intent(applicationContext, ExerciseTypeListActivity::class.java))
                2 -> startActivity(Intent(applicationContext, WorkoutListActivity::class.java))
                3 -> startActivity(Intent(applicationContext, SettingsActivity::class.java))
                else -> Log.wtf(TAG, "This item should not exist.")
            }
        }
    }


    /** Shows a dialog for choosing a {@link Workout} */
    private fun showSelectWorkoutDialog() {
        val dataProvider = DataProvider(this)
        // get all Workouts
        val workoutList = dataProvider.workouts
        Log.d(TAG, "Number of Workouts: " + workoutList.size)
        when (workoutList.size) {
            // show error message, if there is no Workout
            0 -> Toast.makeText(this@MainActivity, getString(R.string.no_workout), Toast.LENGTH_LONG).show()
            // choose Workout, if there is/are one or more Workout(s)
            else -> {
                val ft = supportFragmentManager.beginTransaction()
                val prev = supportFragmentManager.findFragmentByTag("dialog")
                if (prev != null) {
                    ft.remove(prev)
                }
                ft.addToBackStack(null)
                // Create and show the dialog.
                val newFragment = SelectWorkoutDialogFragment.newInstance()
                newFragment.show(ft, "dialog")
            }
        }
    }

}
