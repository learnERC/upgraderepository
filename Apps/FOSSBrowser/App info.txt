FOSSBrowser:
    App package: de.baumann.browser
    Bug Commit: https://github.com/scoute-dich/browser/commit/02b834e09c2471e43a567ac92a5df58b5e1ce0a5
    Issue: https://github.com/scoute-dich/browser/issues/260
    Fix Commit: https://github.com/scoute-dich/browser/commit/62e45864453ecab7e29df0c3d6031bc3b41675f4
    Working version: Android 8.0 API 26
    Bugged version: Android 7.1 API 25
    Bug description: crash after launch
    Fault: faulty support to the previous api
