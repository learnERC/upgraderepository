package dk.example.example_fragment_oninflate;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

//import android.app.Fragment;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private String mText;

    public MainActivityFragment() {
    }


    @Override
    public void onInflate(final Activity activity, final AttributeSet attrs, final Bundle savedInstanceState) {
        //super.onInflate(activity, attrs, savedInstanceState);

        TypedArray a = activity.obtainStyledAttributes(attrs, R.styleable.MyFragmentArguments);
        mText = (String) a.getText(R.styleable.MyFragmentArguments_text);
        a.recycle();

    }


    @Override
    public void onInflate(final AttributeSet attrs, final Bundle savedInstanceState) {
        mText = "Deprecated onInflate called";
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View inflate = inflater.inflate(R.layout.fragment_layout, container, false);

        TextView textView = (TextView) inflate.findViewById(R.id.hello_world_text_view);
        textView.setText(mText);

        return inflate;
    }
}
