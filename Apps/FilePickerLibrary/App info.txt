FilePickerLibrary:
    App package: com.devpaul.filepickerbug
    Bug Commit: https://github.com/DeveloperPaul123/FilePickerLibrary/commit/c7e995867a6006bd4eecc4cc423f8be473996892
    Issue: https://github.com/DeveloperPaul123/FilePickerLibrary/issues/33
           https://github.com/DeveloperPaul123/FilePickerLibrary/issues/34
    Fix Commit: https://github.com/DeveloperPaul123/FilePickerLibrary/commit/213ea56e808e48d269b910cead35ef791bb961d1
    Working version: Android 5.1 API 22
    Bugged version: Android 6.0 API 23
    Bug description: The library does not work on devices with API 23 as using a deprecated method is not called, folders erroneously shown as empty
    Fault: faulty support to the new api
