package com.devpaul.filepicker;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.robotium.solo.Solo;
import com.devpaul.filepicker.R;


/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
@SuppressWarnings("rawtypes")
public class ApplicationTest extends ActivityInstrumentationTestCase2{
    private Solo solo;

    private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.devpaul.filepicker.MainActivity";

    private static Class<?> launcherActivityClass;
    static{
        try {
            launcherActivityClass = Class.forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public ApplicationTest() throws ClassNotFoundException {
        super(launcherActivityClass);
    }

    public void setUp() throws Exception {
        super.setUp();
        solo = new Solo(getInstrumentation());
        getActivity();
    }

    //the test methods MUST begin with test...
    public void testRun() throws Exception{
        //solo.unlockScreen();
        View b = solo.getView(R.id.file_picker_activity);
        solo.clickOnView(b);
        solo.sleep(2000);
        solo.clickOnText("Music");
        solo.clickOnText("Open");
        solo.sleep(2000);
    }

    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }
}